<?php defined('SYSPATH') or die('No direct script access.');

/**
* Validation input params
*/
class Validation
{
    public function is_md5($val)
    {
        $val = (preg_match('/^[a-fA-F0-9]{32}$/', $val)) ? $val : false;
        return $val;
    }

    public function is_social($val)
    {
        $val = (preg_match('/^[a-zA-Z0-9_\-.:]+$/', $val)) ? $val : false;
        return $val;
    }

    public function is_email($val)
    {
        $val = (filter_var($val, FILTER_VALIDATE_EMAIL)) ? $val : false;
        return $val;
    }

    public function is_time($val)
    {
        $val = (preg_match('/^[0-9:]{5}$/', $val)) ? $val : false;
        $val = (strtotime($val) === false) ? false : $val;
        return $val;
    }

    public function to_string($val)
    {
        $val = strip_tags(html_entity_decode($val, ENT_QUOTES));
        $val = preg_replace(array('/</', '/>/', '/=/', '/%/', '/@/'), '', $val);
        $val = (filter_var($val, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW));
        $val = ($val) ? trim($val) : false;

        return $val;
    }
}