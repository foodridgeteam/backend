<?php defined('SYSPATH') or die('No direct script access.');

class MediaController extends Base
{
    function __construct() {
    	parent::__construct();
	}

	/**
     * upload avatar
     *
     * @url POST /upload_avatar
     *
     */
    public function upload_avatar() {
        $user_id = (int) $this->check_auth()->user_id;
        $type = 1;
        $result = $this->upload_media($type , $id); // params: meal_id, meal type
        if ($result['error'] != NULL)
                    return $result;
        $userController = new UserController;

        return array(
                    'data' => $userController->get_user_profile($user_id)
                );
    }

    /**
     * delete avatar
     *
     * @url GET /delete_avatar
     *
     */
    public function delete_avatar() {
        $user_id = (int) $this->check_auth()->user_id;;
        $query = $this->pdo->prepare(
                "SELECT DISTINCT avatar
                 FROM users
                 WHERE id = :id
                 LIMIT 1"
            );
        $query->execute(array('id' => $user_id));
        $avatar_name = $query->fetchColumn();

        $medium = SYSPATH."/api/img/avatar/medium".$avatar_name;
        if(!unlink($medium)){
            return $this->error_deleting_file;

        }else{
            $query = $this->pdo->prepare(
                "UPDATE users
                 SET    avatar = :avatar
                 WHERE  id   = :id
                 LIMIT 1"
            );

            $data_query = array(
                'avatar'   => NULL,
                'id' => $user_id
            );
            $query->execute($data_query);
        }

        return array("data" => "ok");

    }

    /**
     * upload avatar
     *
     * @url POST   /meal_media
     * @url DELETE /meal_media/$name
     */
    public function meal_media($name) {
        $type = 2;  // meal type
        if($_SERVER['REQUEST_METHOD']=="POST"){
            if ($_POST['id'] === null)
                return $this->error_meal_id;

            $result = $this->upload_media($type , (int)$_POST['id']); // params:  meal type ,meal_id,
            if ($result['error'] != NULL)
                        return $result;
        }else{
            $result = $this->delete_media($name);//set image_id
            if ($result['error'] != NULL)
                        return $result;
        }
        return array("data" => "ok");
    }


    private function upload_media($type , $id) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $user_id = (int) $this->check_auth()->user_id;
        if(!$_FILES['file']['tmp_name']){
            return $this->error_fileexist;
        }
        $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG);
        $detectedType = exif_imagetype($_FILES['file']['tmp_name']);
        if(!in_array($detectedType, $allowedTypes)){
            return $this->error_filetype;
        }

        $info_image = getimagesize($_FILES['file']['tmp_name']);

        $rand_str = str_shuffle($characters);
        $createName = md5($rand_str.$user_id);
        $fileType = '.jpg';
        $imgName = $createName.$fileType;

        if($type == 2){
            $media_folder = 'meals';
        }else{
            $media_folder = 'avatar';
        }
        $target = SYSPATH."/api/img/$media_folder/".$imgName;

        try{
            if($info_image['mime']=='image/jpeg'){
                move_uploaded_file($_FILES['file']['tmp_name'], $target);
            }else{
                /*create from png*/
                //new file name once the picture is converted
                $converted_filename = SYSPATH."/api/img/$media_folder/$createName.jpg";

                $new_pic = imagecreatefrompng($_FILES['file']['tmp_name']);
                // Create a new true color image with the same size
                $w = imagesx($new_pic);
                $h = imagesy($new_pic);
                $white = imagecreatetruecolor($w, $h);

                // Fill the new image with white background
                $bg = imagecolorallocate($white, 255, 255, 255);
                imagefill($white, 0, 0, $bg);

                // Copy original transparent image onto the new image
                imagecopy($white, $new_pic, 0, 0, 0, 0, $w, $h);

                $new_pic = $white;

                imagejpeg($new_pic, $converted_filename);
                imagedestroy($new_pic);
                /*create from png*/
            }
            if($type == 2){
                $query = $this->pdo->prepare(
                    "INSERT INTO meal_fotos
                        (meal_id, foto)
                     values
                        (:meal_id, :foto)"
                );

                $insert = array(
                    "meal_id"   => $id,
                    "foto"      => "$createName.jpg"
                );

                $result = $query->execute($insert);

            }else{
                $query = $this->pdo->prepare(
                                                "UPDATE users SET avatar = :avatar
                                                 WHERE id = :id"
                                                );

                $array = array(
                        'avatar' => $imgName,
                        'id'   => $user_id
                    );
                $result = $query->execute($array);
            }
        }catch (Exception $e) {
            //return $e->getMessage();
            return $this->error_upload;
        }
        //crop
        $filename =  SYSPATH."/api/img/$media_folder/medium/$createName.jpg";

        // if new image small then 750 px not crop
        if($info_image[0] > 750 OR $info_image[1] > 750){
            $image = imagecreatefromjpeg($target);
            $thumb_width = 750;
            $thumb_height = 750;

            $width = imagesx($image);
            $height = imagesy($image);

            $original_aspect = $width / $height;
            $thumb_aspect = $thumb_width / $thumb_height;

            if ( $original_aspect >= $thumb_aspect )
            {
               // If image is wider than thumbnail (in aspect ratio sense)
               $new_height = $thumb_height;
               $new_width = $width / ($height / $thumb_height);
            }
            else
            {
               // If the thumbnail is wider than the image
               $new_width = $thumb_width;
               $new_height = $height / ($width / $thumb_width);
            }

            $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

            // Resize and crop
            /*imagecopyresampled($thumb,
                               $image,
                               0, // Center the image horizontally
                               0, // Center the image vertically
                               0, 0,
                               750, 750,
                               $width, $height);*/

            imagecopyresampled($thumb,
                               $image,
                               0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                               0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                               0, 0,
                               $new_width, $new_height,
                               $width, $height);

            imagejpeg($thumb, $filename, 80);
        }else{
            copy($target, $filename);  // just move small image to thumb directory
        }

    }


    //method delete media
    private function delete_media($name){
        $name = $name.".jpg";
        $user_id = (int) $this->check_auth()->user_id;

        $query = $this->pdo->prepare(
                "SELECT DISTINCT meals.user_id
                 FROM meals
                 INNER JOIN meal_fotos ON (meals.id = meal_fotos.meal_id)
                 WHERE meal_fotos.foto = :foto
                 LIMIT 1"
            );
        $query->execute(array('foto' => $name));
        $user_id_owner_foto = $query->fetchColumn();
        if($user_id_owner_foto != $user_id){
            return $this->error_owner_media;
        }

        $medium = SYSPATH."/api/img/meals/medium/".$name;
        if(!unlink($medium)){
            return $this->error_deleting_file;

        }else{
            $query = $this->pdo->prepare(
                "DELETE FROM meal_fotos
                 WHERE foto = :foto"
            );
            $data = array(
                "foto"        => $name
            );
            $result = $query->execute($data);
        }

        return array("data" => "ok");
    }

}
