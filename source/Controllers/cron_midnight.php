<?php $syspath = preg_replace('/\/api\/source\/Controllers/', '', dirname(__FILE__));
define(SYSPATH, $syspath);
require(SYSPATH.'/api/source/Controllers/Base.php');


class Cronmidnight extends Base
{
    function __construct() {
        parent::__construct();
    }

    public function start_midnight()
    {
    	// array of data to db
        $data = array(
                        "active_coupon" => 0 
                    );
		//db query
        $query = $this->pdo->prepare(
                                    "UPDATE coupons SET active_coupon = :active_coupon 
                                     WHERE coupons.date_from  > NOW()
                                     OR coupons.date_to    < NOW() - INTERVAL 1 day"
                                    );
        $result = $query->execute($data);
        
    }


    protected function check_auth(){
        return true;
    }
}
$start = new Cronmidnight();
$start->start_midnight();

$fd = fopen(SYSPATH . "/api/log/cron-midnight.txt","a");
fwrite($fd, "log ".date("d.m.Y H:i:s")."\r\n");
fclose($fd);