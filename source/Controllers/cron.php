<?php $syspath = preg_replace('/\/api\/source\/Controllers/', '', dirname(__FILE__));
define(SYSPATH, $syspath);
require(SYSPATH.'/api/source/Controllers/Base.php');
require(SYSPATH.'/api/source/Controllers/PushController.php');
require(SYSPATH.'/api/source/Controllers/RefundController.php');

class Cron extends Base
{
    function __construct() {
        parent::__construct();
    }

    public function cancel_orders_system()
    {

        // status 1 = created
        // status 9 = system cancel
        $current_date = date('Y-m-d H:i:s');
        $stamp = strtotime($current_date) - 12*60*60;
        $old_date = gmdate('Y-m-d H:i:s', $stamp);

        $allowed_statuses = array(1,2,3);
        $str_statuses = '('.implode(',', $allowed_statuses).')';


        $query = $this->pdo->prepare(
            "SELECT id,
             sender_user_id,
             chef_id,
             delivery_type,
             transaktion_id,
             type_payment
             FROM orders
             WHERE created < :created
             AND  status IN $str_statuses"
        );
        $query->execute(array('created' => $old_date));
        $orders = $query->fetchAll(PDO::FETCH_OBJ);
        if ( ! $orders)
            return false;

        //refund part
        $refund_obj = new RefundController;
        $push = new PushController;

        foreach ($orders as $single_order) {
            /*
            * call stripe refund api only for stripe payments
            */
            if($single_order->type_payment == 1){
               $refund_obj->stripe_make_refund($single_order->transaktion_id, $single_order->id);
            }
            $push->push_set_cron($single_order->id, 9, $single_order->sender_user_id, $single_order->chef_id, $delivery_type);
        }
        //refund part

        $query = $this->pdo->prepare(
            "UPDATE orders
             SET status = 9
             WHERE status IN $str_statuses
             AND created < :created"
        );

        try {
            $query->execute(array('created' => $old_date));

            // вызов пушей ( отослать повару и юзеру )

        } catch (Exception $e) {
            return false;
        }
    }


    protected function check_auth(){
        return true;
    }
}
$start = new Cron();
$start->cancel_orders_system();

$fd = fopen(SYSPATH . "/api/log/cron.txt","a");
fwrite($fd, "log ".date("d.m.Y H:i:s")."\r\n");
fclose($fd);