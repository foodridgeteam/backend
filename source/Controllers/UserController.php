<?php defined('SYSPATH') or die('No direct script access.');

require 'Base.php';
include_once SYSPATH.'/api/lib/SendMailSmtpClass.php';

/*functions
*
* public function chefs
* public function get_delivery_info
* public function register
* public function login
* public function check_email_by_token
* public function get_user
* public function get_user_profile
* public function logout
* public function user_profile
* public function terms
* public function set_pushid
* public function follow_chef
* public function get_chef 
* public function chef_profile
* public function get_chef_profile
* public function comment
* public function comments
* public function feedback
* public function recommend
* public function following
* public function followed
* public function invite
* public function push_period
* 
* 
* 
*/

class UserController extends Base
{

    private $pushId;
    private $email;
    private $username;
    private $password;
    private $device_type;
    private $social_user_id;
    private $social_type;
    private $social_token;
    private $push_flag;

    private $list_chefs_full = null;

    private $uid;

    function __construct() {
         parent::__construct();

        $json = file_get_contents('php://input');
    	$obj_post = json_decode($json);

    	$this->pushId          = ($obj_post->pushId)         ? $this->validation->is_social($obj_post->pushId)            : false;
        $this->push_flag       = ($obj_post->push_flag)      ? substr( (int) $obj_post->push_flag, 0, 1)                  : false;
        $this->email           = ($obj_post->email)          ? $this->validation->is_email($obj_post->email)              : false;
        $this->username        = ($obj_post->username)       ? $this->validation->to_string($obj_post->username)          : false;
        $this->password        = ($obj_post->password)       ? $this->validation->is_md5($obj_post->password)             : false;
        $this->device_type     = ($obj_post->deviceType)     ? substr( (int) $obj_post->deviceType, 0, 1)                 : false;
        $this->social_user_id  = ($obj_post->socialUserId)   ? $this->validation->is_social($obj_post->socialUserId)      : false;
        $this->social_type     = ($obj_post->socialType)     ? substr( (int) $obj_post->socialType, 0, 1)                 : false;
        $this->social_token    = ($obj_post->socialToken)    ? $this->validation->is_social($obj_post->socialToken)       : false;
        $this->device_id       = ($obj_post->deviceId)       ? $this->validation->is_social($obj_post->deviceId)          : false;

        if ($_GET['limit']){
        	if ($this->list_chefs_full == null){
                    if($_GET['latitude'] AND $_GET['longitude']){
	        		$latitude 	= round($_GET['latitude'], 5);
		        	$longitude 	= round($_GET['longitude'], 5);
                }
		        	$this->list_chefs_full = $this->construct_chefs_full($latitude , $longitude);
	        	}
        }

	}



    private function update_visit($user_id)
    {
        $query = $this->pdo->prepare(
            "UPDATE users
             SET last_visit = :last_visit
             WHERE id = :id
             LIMIT 1"
        );
        $update = array(
            'id'         => $user_id,
            'last_visit' => gmdate("Y-m-d H:i:s")
        );
        $result = $query->execute($update);
    }



    public function construct_chefs_full($latitude = NULL, $longitude = NULL, $chef_ids = NULL) {

    	if($latitude == NULL AND $longitude == NULL)
        {
	    	$user_id = (int) $this->check_auth()->user_id;
	    	$query = $this->pdo->prepare(
	            "SELECT DISTINCT latitude, longitude
	             FROM delivery
	             WHERE user_id = :user_id
	             LIMIT 1"
	        );
			$query->execute(array('user_id' => (int) $user_id));
			$coords 	= $query->fetch();
			$latitude 	= $coords->latitude;
			$longitude 	= $coords->longitude;
		}

    	$sql = "SELECT chefs.user_id,
             chefs.license,
             chefs.about,
             chefs.business_site,
             chefs.business_name,
             chefs.business_phone,
             chefs.pickup_country,
             chefs.pickup_city,
             chefs.pickup_street,
             chefs.rating,
             chefs.votes,
             chefs.latitude,
             chefs.longitude,
             chefs.followers,
             users.avatar,
             users.name,
             users.last_visit ";
        /*
        * if have by distance query
        */
        if($latitude != NULL AND $longitude != NULL)
        {
        	$sql .= ",( 6371 * acos( cos( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( latitude ) ) ) ) AS distance ";
        }

        $sql .= "FROM chefs
	             INNER JOIN users ON (users.id = chefs.user_id) ";
        if($chef_ids)
        {   
            $ids = join("','",$chef_ids);
            $sql .= " WHERE chefs.user_id IN ('$ids') ";
        }
        /*
        *  if order by distance , or not
        */
        if($latitude != NULL AND $longitude != NULL)
        {
        	$sql .= "HAVING distance < ".DEFAULTRADIUS."
	             	 ORDER BY distance ASC";
        }else
        {
        	$sql .= "ORDER BY chefs.user_id ASC";
        }

        $query = $this->pdo->prepare($sql);
        $query->execute();
        $chefs = $query->fetchAll(PDO::FETCH_OBJ);

        return $chefs;
    }


    /**
     * @url GET /chefs
     *
     */
    public function chefs()
    {
        $array   	= array();
        $limit   	= (int) $_GET['limit'];
        $offset  	= (int) $_GET['offset'];

        $search  = ($_GET['search']) ? $this->validation->to_string($_GET['search']) : false;

        if ($_GET['limit'])
            $list_chefs = $this->list_chefs_full;

        $couponController = new CouponController;
        
        foreach ($list_chefs as $key => $chef) {
            //if(!(int)$chef->latitude OR !(int)$chef->longitude){continue;}
            $full_avatar_path = NULL;
            if($chef->avatar != NULL){
                $full_avatar_path = METHOD."://".$_SERVER['HTTP_HOST'].'/api/img/avatar/medium/'.$chef->avatar;
            }
            $temp = array(
            	//'distance'		=> $chef->distance,
                'id'            => (int) $chef->user_id,
                'avatar'        => $full_avatar_path,
                'name'          => $chef->name,
                'about'         => $chef->about,
                'rating'        => (float) $chef->rating,
                'votesCount'    => (int) $chef->votes,
                'followers'     => (int) $chef->followers,
                'lastVisit'     => $chef->last_visit,
                'deliveryInfo'  => array(
                    'country'       => $chef->pickup_country,
                    'city'          => $chef->pickup_city,
                    'street'        => $chef->pickup_street,
                    'distance'      => $chef->distance
                ),
                'businessInfo'  => array(
                    'name'              => $chef->business_name,
                    'registrationId'    => $chef->license,
                    'site'              => $chef->business_site,
                    'phone'             => $chef->business_phone
                ),
                'workingTime'   => $this->working( (int) $chef->user_id),
                'cuisines'      => $this->get_chef_cuisines( (int) $chef->user_id),
                'follow'        => $this->get_follow( (int) $chef->user_id),
                'coupon'        => $couponController->get_user_coupon((int) $chef->user_id)
            );
            if($chef->distance != NULL){
                $temp['deliveryInfo']['latitude']  = $chef->latitude;
                $temp['deliveryInfo']['longitude'] = $chef->longitude;
            }
            if ($search) {
                if (stripos($chef->name, $search) !== FALSE ) {
                    $array[] = $temp;
                }
            } else {
                $array[] = $temp;
            }
        }

        $array_offset = array_chunk($array, $limit);

        return array('data' => $array_offset[$offset]);
    }


    private function secure_pass($pass) {
        // TO DO: 2 version
        //$pass = strtolower($pass);
        return hash('sha512', md5(SALT.$pass));
    }


    private function generate_access_token() {
        return md5(uniqid(mt_rand(), true));
    }


    public function get_delivery_info($user_id) {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT country, city, street, more, latitude, longitude
             FROM delivery
             WHERE user_id = :user_id
             LIMIT 1"
        );

        $query->execute(array('user_id' => (int) $user_id));

        $delivery = $query->fetch();

        $delivery = ($delivery) ? array(
							            'country' 	=> $delivery->country,
							            'city'    	=> $delivery->city,
							            'street'  	=> $delivery->street,
							            'more'    	=> $delivery->more,
							            'latitude'  => $delivery->latitude,
							            'longitude' => $delivery->longitude
								        )
        : null;

        return $delivery;
    }

	private function check_password($email, $password) {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT id
             FROM users
             WHERE email = :email
             AND password = :password
             LIMIT 1"
        );
        $query->execute(array('email' => $email, 'password' => $password));
        $user_id = $query->fetchColumn();

        return $user_id;
    }

    private function update_device_id($token, $user_id){
        $query = $this->pdo->prepare(
            "DELETE FROM sessions WHERE device_id = :device_id"
        );

        try {
            $query->execute(array('device_id' => $this->device_id));
        } catch (Exception $e) {}

        $query = $this->pdo->prepare(
            "INSERT INTO sessions
                (token,  user_id, device_id , device_type)
             values
                (:token,  :user_id, :device_id , :device_type)"
            );

        $data = array(
            "token"         => $token,
            "user_id"       => $user_id,
            "device_id"     => $this->device_id,
            "device_type"   => $this->device_type
        );
        try{
            $result = $query->execute($data);
        } catch (Exception $e) {
                return $this->db_error;
        }
    }


    private function login_basic() {
        if ( ! $this->email) return $this->error_email;
        if ( ! $this->device_type) return $this->error_device_type;
        if ( ! $this->device_id ) return $this->error_device_id;

        $user_id = $this->check_password($this->email,$this->secure_pass($this->password));
        if ( ! $user_id) return $this->error_password;

        $application_access_token = $this->generate_access_token();

        $this->update_device_id($application_access_token, $user_id);

        return array(
            'data' => array(
                'token'       => $application_access_token,
                'userProfile' => $this->get_user_profile($user_id)
            )
        );

    }

    /**
     * @url POST /register
     *
     */
    public function register() {
        if ( ! $this->email) return $this->error_email;
        if ( ! $this->username) return $this->error_username;
        if ( ! $this->password) return $this->error_password;
        if ( ! $this->device_type ) return $this->error_device_type;
        if ( ! $this->device_id ) return $this->error_device_id;

        $application_access_token = $this->generate_access_token();

        $query = $this->pdo->prepare(
            "INSERT INTO users
                (name,  password, email, role_id, last_visit)
             values
                (:name,  :password, :email, :role_id, :last_visit)"
        );

        $data = array(
            "name"          => $this->username,
            "password"      => $this->secure_pass($this->password),
            "email"         => $this->email,
            "role_id"       => $this->role_user,
            "last_visit"    => gmdate("Y-m-d H:i:s")
        );

        try {
            $result  = $query->execute($data);
            $user_id = $this->pdo->lastInsertId();

            $this->update_device_id($application_access_token, $user_id);

            return array(
                'data' => array(
                    'token'       => $application_access_token,
                    'userProfile' => $this->get_user_profile($user_id)
                )
            );

        } catch (Exception $e) {
        	return $this->error_not_uniq_email;
        	// return $e->getMessage();
        }
    }


    /**
     * @url POST /login
     *
     */
    public function login() {
    	if ( ! $this->social_token) {
            return $this->login_basic();
        } else {
            return $this->login_social();
        }
    }

    /**
    *   social part
    ***/

    // social sign method
    private function login_social() {
        if ( ! $this->device_id) return $this->error_device_id;
        //if ( ! $this->social_user_id) return $this->error_social_user_id;
        if ( ! $this->social_type ) return $this->error_social_type;
        if ( ! $this->social_token) return $this->error_social_token;
        if ( ! $this->device_type ) return $this->error_device_type;

        $application_access_token = $this->generate_access_token();
        if($this->social_type == 1){//fb part
                $user_id = $this->sign_facebook($application_access_token , $this->device_id, $this->device_type, $this->social_user_id, $this->social_token);
                $fd = fopen(SYSPATH . "/api/log/error_fb_login.txt","a");
                fwrite($fd, date("d.m.Y H:i:s"). ", error = ". $user_id . "\r\n");
                fclose($fd);
                if($user_id == false){
                    return $this->error_facebook;
                }
        //else
        }else{// G+ part
                $rand_avatar_id = mt_rand(10000000, 100000000);
                $user_id = $this->sign_google($application_access_token , $this->device_id, $this->device_type, $rand_avatar_id, $this->social_token);
        }
        if($user_id != false){
            //return json user object
            return array(
                'data' => array(
                    'token'       => $application_access_token,
                    'userProfile' => $this->get_user_profile($user_id)
                )
            );

        }else{
            return $this->error_social_login;
            //return $e->getMessage();
        }
    }

    //sign from facebook
    private function sign_facebook($application_access_token , $device_id, $device_type, $uid, $socialToken){
        //prepare data to fb
        $graph_url= "https://graph.facebook.com/me?fields=id,name,email,cover&access_token=$socialToken";

        //execute curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $graph_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $facebook_user = json_decode(urldecode(curl_exec($ch)));

        curl_close($ch);

            $fd = fopen(SYSPATH . "/api/log/error_fb_login2.txt","a");
            fwrite($fd, date("d.m.Y H:i:s"). ", error = ". $graph_url . "\r\n");
            fclose($fd);
        $uid       = $facebook_user->id;
        $email     = $facebook_user->email;
        $username  = $facebook_user->name;

        if($email){//if no error from facebook
            //generate token
            $image_url = "https://graph.facebook.com/$uid/picture?type=large&redirect=false&access_token=$socialToken";
            $user_id = $this->unique_email($email);// get user_id by mail
            if($user_id != NULL){ //if already exist email , update token and login
                $this->update_device_id($application_access_token , $user_id);
            }else{ //if no exist mail register new user
            	//get facebook image
	            $image_url = $this->check_fb_avatar($image_url);
                //image part
                if($image_url != FALSE){
                    $this->download_image($image_url, $_SERVER['DOCUMENT_ROOT']."/api/img/avatar/medium/avatar_$uid.jpg");
                	$this->download_image($image_url, $_SERVER['DOCUMENT_ROOT']."/api/img/avatar/avatar_$uid.jpg");
                	$avatar_user = "avatar_$uid.jpg";
                }
	            $user_id = $this->register_social_user($application_access_token, $username, $avatar_user, $email , $device_id, $device_type);
            }
            return $user_id;
        }else{
            return false;
        }
    }


    //sign from google
    private function sign_google($application_access_token , $device_id, $device_type, $uid, $socialToken) {

        $url = 'https://www.googleapis.com/plus/v1/people/me?access_token='.$socialToken;
        //execute curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $google_plus_user = json_decode(curl_exec($ch));
        curl_close($ch);

        if($google_plus_user->error == NULL){//if no error from google
            //generate token
            //get user data from google
            $email = $google_plus_user->emails[0]->value;
            $username  = $google_plus_user->displayName;
            $user_id = $this->unique_email($email);// get user_id by mail
            if($this->unique_email($email) != NULL){ //if already exist email , update token
                $this->update_device_id($application_access_token , $user_id);
            }else{//if no exist mail register new user
            	//get image from G+ if it's NOT DEFAULT image
	            if($google_plus_user->image->isDefault == FALSE){
	                //image part
	                $file = $google_plus_user->image->url;
                    $this->download_image($file, $_SERVER['DOCUMENT_ROOT']."/api/img/avatar/medium/avatar_$uid.jpg");
	                $this->download_image($file, $_SERVER['DOCUMENT_ROOT']."/api/img/avatar/avatar_$uid.jpg");
                    $avatar_user = "avatar_$uid.jpg";
	            }
                $user_id = $this->register_social_user($application_access_token, $username, $avatar_user, $email , $device_id, $device_type);
            }
            return $user_id;
        }else{
            return false;
        }
    }

    //register new social user
    private function register_social_user($application_access_token, $username, $avatar_user, $email , $device_id, $device_type){

        $query = $this->pdo->prepare(
            "INSERT INTO users
                (name, email, role_id, avatar, last_visit)
             values
                (:name, :email, :role_id, :avatar, :last_visit)"
        );

        $data = array(
            "name"          => $username,
            "email"         => $email,
            "avatar"        => $avatar_user,
            "role_id"       => $this->role_user,
            "last_visit"    => gmdate("Y-m-d H:i:s")
        );
            $result = $query->execute($data);
            $id = $this->pdo->lastInsertId();
            $this->update_device_id($application_access_token , $id);

            return $id;
    }

    /**
     *
     * @url POST /check_email_by_token
     *
     */
    public function check_email_by_token($data)
    {
        if ( ! $this->social_type ) return $this->error_social_type;
        if ( ! $this->social_token) return $this->error_social_token;
        $socialToken = $this->social_token;
        
        if($this->social_type == 1) //fb check
        {
            //prepare data to fb
            $graph_url= "https://graph.facebook.com/me?fields=id,name,email,cover&access_token=$socialToken";

            //execute curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $graph_url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            
            $facebook_user = json_decode(urldecode(curl_exec($ch)));
            
            curl_close($ch);

            $email = $facebook_user->email;
            if(!$email)//if error from facebook
            {
                return $this->error_facebook_token;
            }

        }else
        {   // google check
            $url = 'https://www.googleapis.com/plus/v1/people/me?access_token='.$socialToken;
            //execute curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            $google_plus_user = json_decode(curl_exec($ch));
            curl_close($ch);
            if($google_plus_user->error == NULL)
            {//if no error from google
                $email = $google_plus_user->emails[0]->value;
            } else
            {
                return $this->error_google_token;
            }
        }
        $user_id = $this->unique_email($email);// get user_id by mail
        if(!$user_id) 
        {
            $answer = array ('data' => null);
        }
        else{
            $answer = array(
                       'data' => array('user_id' => $user_id)
                     );
        }
        
        return $answer;
    }

    //get image avatar from social
    private function download_image($image_url, $image_file){
            $fp = fopen ($image_file, 'w+');              // open file handle

            $ch = curl_init($image_url);
            curl_setopt($ch, CURLOPT_FILE, $fp);          // output to file
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 1000);      // some large value to allow curl to run for a long time
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
            curl_exec($ch);
            curl_close($ch);                              // closing curl handle
            fclose($fp);                                  // closing file handle
            return true;
    }

    //check already exist email , return uid
    protected function unique_email($email_to_check) {
        $query = $this->pdo->prepare("SELECT id FROM users WHERE email = :email");
        $query->execute(array(':email' => $email_to_check));
        $email_already_exist = $query->fetchColumn();
        return $email_already_exist;
    }

    //check default or not fb avatar
    // return - url is not default,  false if default
    private function check_fb_avatar($graph_url) {
        //execute curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $graph_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $facebook_image = json_decode(urldecode(curl_exec($ch)));
        curl_close($ch);
        if( $facebook_image->data->is_silhouette == FALSE){ // not default image on FB
            return str_replace('&redirect=false','',$graph_url);
        } else { // default image on FB
            return FALSE;
        }
    }

    /*social part end*/

    /*USER PROFILE PART*/

    /**
     * @url GET /get_user/$id
     *
     */
    public function get_user($id) {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT id, avatar, name, phone, type_of_daily_push
             FROM users
             WHERE id = :id
             LIMIT 1"
        );
        $query->execute(array('id' => $id));
        $user = $query->fetch();

        if($user == NULL){
            return array("error" => array("code" => 404, "message" => "User not found"));
        }

        if($user->avatar != NULL){
            $full_avatar_path = METHOD."://".$_SERVER['HTTP_HOST'].'/api/img/avatar/medium/'.$user->avatar;
        }

        return array(
            'id'           => (int)$user->id,
            'avatar'       => $full_avatar_path,
            'name'         => $user->name,
            'phone'        => $user->phone,
            'pushFrequency'	 => $user->type_of_daily_push
        );
    }
    // get my own user profile
    public function get_user_profile($user_id) {
        
        $query = $this->pdo->prepare(
            "SELECT DISTINCT users.id, users.avatar, users.name, users.email, users.phone, users.terms , users.type_of_daily_push, chefs.chef_type
             FROM users
             LEFT JOIN chefs ON (chefs.user_id = users.id)
             WHERE id = :user_id
             LIMIT 1"
        );
        $query->execute(array('user_id' => $user_id));
        $user = $query->fetch();

        if ($user) $this->update_visit($user_id);


        $full_avatar_path = NULL;
        if($user->avatar != NULL){
        	$full_avatar_path = METHOD."://".$_SERVER['HTTP_HOST'].'/api/img/avatar/medium/'.$user->avatar;
        }

        //get array of users ids
        $query = $this->pdo->prepare(
                                    "SELECT follow_user_id
                                     FROM followers
                                     WHERE user_id = :user_id"
                                    );
        $query->execute(array(
                            'user_id'        => $user_id,
                            ));
        $follow_arr = $query->fetchAll(PDO::FETCH_COLUMN);


        //get array of users ids who follow to chefId
        $query = $this->pdo->prepare(
                                    "SELECT user_id
                                     FROM followers
                                     WHERE follow_user_id = :follow_user_id"
                                    );
        $query->execute(array(
                            'follow_user_id'        => $user_id,
                            ));
        $followed_arr = $query->fetchAll(PDO::FETCH_COLUMN);

        return array(
            'id'           => (int) $user->id,
            'avatar'       => $full_avatar_path,
            'name'         => $user->name,
            'email'        => $user->email,
            'phone'        => $user->phone,
            'agreeTerms'   => (bool)$user->terms,
            'my_following' => count($follow_arr),
            'my_followed'  => count($followed_arr),
            'deliveryInfo' => $this->get_delivery_info($user->id),
            'pushFrequency'	 => (int)$user->type_of_daily_push,
            'chefType'    => (int)$user->chef_type	
        );
    }



    /**
     * @url GET /logout
     *
     */
    public function logout() {
        $query = $this->pdo->prepare(
                "DELETE FROM sessions WHERE token = :token"
            );
        $query->execute(array('token' => $this->check_auth()->token));

        return array("data" => "ok");
    }

    /**
     * user profile routing method
     *
     * @url PUT /user_profile
     * @url GET /user_profile
     *
     */
    public function user_profile($data) {
        $user_id = (int) $this->check_auth()->user_id;

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'PUT':
                $answer = $this->update_user_profile($user_id, $data);

                if ($answer['error'] != NULL)
                    return $answer;

                $answer = array(
                    'data' => $this->get_user_profile($user_id)
                );

                return $answer;
                break;

            default:
                $answer = array(
                    'data' => $this->get_user_profile($user_id)
                );
                return $answer;
                break;
        }

    }

    /**
     * user update terms method
     *
     * @url PUT /terms
     *
     */
    public function terms($data) {
        $user_id = (int) $this->check_auth()->user_id;

        $query = $this->pdo->prepare(
                    "UPDATE users
                     SET terms = :terms
                     WHERE id = :id
                     LIMIT 1"
                );
                $update = array(
                    'terms' => (bool)$data->agreeTerms,
                    'id'    => $user_id
                );
        try{
            $result = $query->execute($update);
        } catch (Exception $e) {
            return $this->db_error;
        }

        return array("data" => "ok");
    }


     private function update_user_profile($user_id, $data)
    {
        if($data->userProfile->name || $data->userProfile->phone){

            $array = array('id' => $user_id);

            if($data->userProfile->name){
                $name   = $this->validation->to_string($data->userProfile->name);
                if ( ! $name)  return $this->error_username;
                $array['name'] = html_entity_decode($name, ENT_QUOTES);
                $sql_update = "name = :name";
            }
            if($data->userProfile->phone){
                $phone  = $this->validation->to_string($data->userProfile->phone);
                if ( ! $phone) return $this->error_phone;
                $array['phone'] = $phone;
                if($sql_update){ $sql_update.= ", ";}
                $sql_update .= "phone = :phone";
            }
            $query = $this->pdo->prepare(
                "UPDATE users SET $sql_update
                 WHERE id = :id"
            );

            try{
                $result = $query->execute($array);
            } catch (Exception $e) {
                return $this->db_error;
            }
        }

        $request = true;

        if ($data->userProfile->deliveryInfo) {
        	$latitude 	= $data->userProfile->deliveryInfo->latitude;
        	$longitude 	= $data->userProfile->deliveryInfo->longitude;
        	/*
			* check coordinates data , before work with database
        	*/
        	if((!is_numeric($latitude) OR !is_numeric($longitude)) && $latitude != NULL AND $longitude != NULL){
        		return $this->error_coordinates;
        	}
			$query = $this->pdo->prepare(
                "SELECT DISTINCT user_id
                 FROM delivery
                 WHERE user_id = :user_id
                 LIMIT 1"
            );
            $query->execute(array('user_id' => $user_id));
            $isset_delivery = $query->fetch();

            if ( ! $isset_delivery) {
                $request = $this->insert_delivery_info($user_id, $data->userProfile->deliveryInfo);
            } else {
                $request = $this->update_delivery_info($user_id, $data->userProfile->deliveryInfo);
            }
        }

        return $request;
    }


    private function insert_delivery_info($user_id, $obj_params)
    {
        $country = $this->validation->to_string($obj_params->country);
        $city    = $this->validation->to_string($obj_params->city);
        $street  = $this->validation->to_string($obj_params->street);
        $more    = $this->validation->to_string($obj_params->more);
        $latitude  = round($obj_params->latitude, 5);
        $longitude = round($obj_params->longitude, 5);

        //if ( ! $country OR ! $city OR ! $street)
            //return $error_delivery;
        if ( ! $country) { $country = null;}
        if ( ! $city)    { $city = null;}
        if ( ! $street)  { $street = null;}
        if ( ! $more)    { $more = null; }

        $query = $this->pdo->prepare(
            "INSERT INTO delivery
                (user_id, country, city , street, more, latitude, longitude)
             values
                (:user_id, :country, :city , :street, :more, :latitude, :longitude)"
            );

        $params = array(
            "user_id" 	=> $user_id,
            "country" 	=> $country,
            "city"    	=> html_entity_decode($city, ENT_QUOTES),
            "street"  	=> html_entity_decode($street, ENT_QUOTES),
            "more"    	=> $more,
            "latitude"	=> $latitude,
            "longitude" => $longitude
        );
        try{
            $insert = $query->execute($params);
        } catch (Exception $e) {
                return $this->db_error;
        }
    }


    private function update_delivery_info($user_id, $obj_params)
    {
        $params = (array) $obj_params;
        $correct_params = array();

        foreach ($params as $key => $value) {
            //if ($this->validation->to_string($value))
                $correct_params[$key] = $value;
        }

        $correct_key = array_keys($correct_params);

        $sql  = 'UPDATE delivery SET ';
        foreach ($correct_key as $key) {
            $sql .= $key . ' = ' . ':' . $key;
            if ($key != end($correct_key))
                $sql .= ', ';
        }
        $sql .= ' WHERE user_id = :user_id';

        $query = $this->pdo->prepare("$sql");

        $correct_params['user_id'] = $user_id;
        try{
            $result = $query->execute($correct_params);
        } catch (Exception $e) {
            return $this->db_error;
        }
    }



    /**
     * set push id device
     *
     * @url POST /set_pushid
     *
     */
    public function set_pushid() {

        if (!$this->pushId) return $this->error_pushId;
        if (!$this->push_flag) return $this->error_push_flag;
        $token = $this->check_auth()->token;
        try{
	        $query = $this->pdo->prepare(
	            "UPDATE sessions
	             SET    push_id = :push_id, push_flag = :push_flag
	             WHERE  token   = :token
	             LIMIT 1"
	        );

	        $data_query = array(
	            'push_id'   => $this->pushId,
	            'push_flag' => $this->push_flag,
	            'token'     => $token
	        );
			$result = $query->execute($data_query);
		} catch (Exception $e) {
			return $this->db_error;
        }
        return array("data" => "ok");
    }




    public function working($user_id)
    {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT user_id
             FROM working
             WHERE user_id = :user_id
             LIMIT 1"
        );
        $query->execute(array('user_id' => $user_id));
        $user_id = $query->fetchColumn();

        if ( ! $user_id)
            return null;

        $working = array();
        $query = $this->pdo->prepare(
            "SELECT weekday.day, working.start, working.end
             FROM weekday
             LEFT JOIN working
             ON (working.weekday_id = weekday.id
             AND working.user_id = :user_id)"
        );
        $query->execute(array('user_id' => $user_id));
        $days = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($days as $key => $value) {
            $working[$value->day] = (! $value->start)
            ? null
            : array(
                'start' => date('H:i', strtotime($value->start)),
                'end'   => date('H:i', strtotime($value->end))
            );
        }

        return $working;
    }


    private function system_days()
    {
        $system_days = array();

        $query = $this->pdo->query(
            "SELECT day, id
             FROM weekday"
        );
        $days = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($days as $key => $value) {
            $system_days[$value->day] = $value->id;
        }

        return $system_days;
    }


    private function update_working($data)
    {
        $errors_days   = false;
        $days          = array();
        $user_id       = $this->check_auth()->user_id;
        $input_working = (array) $data;

        $query = $this->pdo->prepare(
            "SELECT weekday.day, working.start, working.end
             FROM weekday
             LEFT JOIN working
             ON (working.weekday_id = weekday.id
             AND working.user_id = :user_id)"
        );
        $query->execute(array('user_id' => $user_id));
        $db_days = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($db_days as $key => $value) {
            $days[$value->day] = (! $value->start)
            ? null
            : array(
                'start' => date('H:i', strtotime($value->start)),
                'end'   => date('H:i', strtotime($value->end))
            );
        }

        foreach ($input_working as $day => $time) {
            $weekday_id = $this->system_days()[$day];

            if ($time === null AND $days[$day] !== $time) {
                $query = $this->pdo->prepare(
                    "DELETE FROM working
                     WHERE user_id = :user_id
                     AND weekday_id = :weekday_id"
                );
                $query->execute(array(
                    'user_id'    => $user_id,
                    'weekday_id' => $weekday_id
                ));
            }


            $start = ($this->validation->is_time($time->start)) ? $time->start : false;
            $end   = ($this->validation->is_time($time->end))   ? $time->end   : false;
            if ( $time !== null AND (! $start OR ! $end)) {
                $errors_days = $this->error_time;
            }

            if ($days[$day] === null AND $days[$day] !== $time AND $start AND $end) {
                $query = $this->pdo->prepare(
                    "INSERT INTO working
                        (user_id, weekday_id, start , end)
                     values
                        (:user_id, :weekday_id, :start , :end)"
                );

                $insert = array(
                    "user_id"    => $user_id,
                    "weekday_id" => $weekday_id,
                    "start"      => $start,
                    "end"        => $end
                );

                $result = $query->execute($insert);
            }


            if ($days[$day] !== null AND $days[$day] !== $time AND $start AND $end) {
                $query = $this->pdo->prepare(
                    "UPDATE working
                     SET start = :start, end = :end
                     WHERE user_id = :user_id AND weekday_id = :weekday_id
                     LIMIT 1"
                );
                $update = array(
                    'start'      => $start,
                    'end'        => $end,
                    'user_id'    => $user_id,
                    'weekday_id' => $weekday_id
                );
                $result = $query->execute($update);
            }
        }

        if ($errors_days !== false)
            return $errors_days;

        return $this->working($user_id);
    }

    /**
     * функция возвращает флаг follow или нет по id пользователя
     * @return bool - возвращает boolean - follow or not
     */
    public function get_follow($follow_user_id)
    {
        $user_id = $this->check_auth()->user_id;

        $query = $this->pdo->prepare(
            "SELECT DISTINCT follow_user_id
             FROM followers
             WHERE user_id = :user_id AND follow_user_id = :follow_user_id
             LIMIT 1"
        );
        $query->execute(array(
            'user_id'        => $user_id,
            'follow_user_id' => $follow_user_id
        ));
        $yes_follow = ($query->fetchColumn()) ? true : false;

        return $yes_follow;
    }



    /**
     *
     * @url POST /follow_chef
     *
     */
    public function follow_chef($data) {
        $user_id = $this->check_auth()->user_id;

        if ($data->follow === null OR $data->chefId === null)
            return $this->error_follow;

        $follow         = (bool)$data->follow;
        $follow_user_id = (int) $data->chefId;

        if ($follow === false) {
            $query = $this->pdo->prepare(
                "DELETE FROM followers
                 WHERE user_id = :user_id
                 AND follow_user_id = :follow_user_id"
            );
            $data = array(
                "user_id"        => $user_id,
                "follow_user_id" => $follow_user_id
            );
            try{
                $result = $query->execute($data);
            } catch (Exception $e) {
                return $this->db_error;
            }

        } else {

            $query = $this->pdo->prepare(
                "INSERT INTO followers
                    (user_id, follow_user_id)
                 values
                    (:user_id, :follow_user_id)"
            );
            $data = array(
                "user_id"        => $user_id,
                "follow_user_id" => $follow_user_id
            );
            try {
                $result = $query->execute($data);
            } catch (Exception $e) {
                return $this->error_set_follow;
            }

        }

        return $this->get_chef($follow_user_id);
    }

    /**
     * chef
     *
     * @url GET /get_chef/$user_id
     *
     */
    public function get_chef($user_id) {

        $couponController = new CouponController;

        $user_id = (int) $user_id;

        $query = $this->pdo->prepare(
            "SELECT DISTINCT users.id,
             users.avatar,
             users.name,
             users.last_visit,
             chefs.about,
             chefs.license,
             chefs.business_site,
             chefs.business_name,
             chefs.business_phone,
             chefs.pickup_country,
             chefs.pickup_city,
             chefs.pickup_street,
             chefs.rating,
             chefs.votes,
             chefs.followers,
             chefs.stripe_client_id,
             chefs.payment_cash,
             chefs.latitude,
             chefs.longitude
             FROM users
             INNER JOIN chefs ON (chefs.user_id = users.id)
             WHERE id = :user_id
             LIMIT 1"
        );
        $query->execute(array('user_id' => $user_id));
        $chefs = $query->fetch();

        $full_avatar_path = NULL;
        if($chefs->avatar != NULL){
            $full_avatar_path = METHOD."://".$_SERVER['HTTP_HOST'].'/api/img/avatar/medium/'.$chefs->avatar;
        }

        return array(
            'data' => array(
                'id'            => (int) $chefs->id,
                'avatar'        => $full_avatar_path,
                'name'          => $chefs->name,
                'about'         => $chefs->about,
                'rating'        => (float) $chefs->rating,
                'votesCount'    => (int) $chefs->votes,
                'followers'     => (int) $chefs->followers,
                'lastVisit'     => $chefs->last_visit,
                'deliveryInfo'  => array(
                    'country'       => $chefs->pickup_country,
                    'city'          => $chefs->pickup_city,
                    'street'        => $chefs->pickup_street,
                    'latitude'		=> $chefs->latitude,
                    'longitude'		=> $chefs->longitude,
                ),
                'businessInfo'  => array(
                    'name'              => $chefs->business_name,
                    'registrationId'    => $chefs->license,
                    'site'              => $chefs->business_site,
                    'phone'             => $chefs->business_phone
                ),
                'workingTime'   => $this->working($user_id),
                'cuisines'      => $this->get_chef_cuisines($user_id),
                'follow'        => $this->get_follow($user_id),
                'paymentInfo'   => array(
                                    'stripeId'    => $chefs->stripe_client_id,
                                    'paymentCash' => (bool)$chefs->payment_cash
                                ),
                'coupon'        => $couponController->get_user_coupon((int) $chefs->id)
            )
        );
    }



	/**
     * user profile routing method
     *
     * @url PUT /chef_profile
     * @url GET /chef_profile
     *
     */
    public function chef_profile($data) {
        $user_id = (int) $this->check_auth()->user_id;
        //unset($data->chefProfile->deliveryInfo->more);

        if ($_SERVER['REQUEST_METHOD'] != 'GET'){ // PUT part
            $cuisines_param = $data->chefProfile->cuisines;
            if ($data === null OR $data->chefProfile === null)
                return $this->error_json;

            $answer = $this->update_working($data->chefProfile->workingTime);
            if ($answer['error'] != NULL)
                return $answer;

            //update chef
            $cuisines = $data->chefProfile->cuisines;

            unset($data->chefProfile->workingTime);
            unset($data->chefProfile->cuisines);
            $data = (array) $data->chefProfile;
            if($data){
	            $answer = $this->update_chef_profile($user_id, $data);
	            if ($answer['error'] != NULL)
	                return $answer;
            }
            if($cuisines != NULL){
            	$answer = $this->set_chef_cuisines($cuisines, $user_id);
        	}
            if ($answer['error'] != NULL)
                return $answer;


        }

        return array(
            'data' =>  $this->get_chef_profile($user_id)
        );
    }
    /*-----------------chef profile------------*/

    private function set_chef_cuisines($data ,$user_id){
        try{
            $query = $this->pdo->prepare(
                "SELECT cuisines_id
                 FROM cuisines_chefs
                 WHERE user_id = :user_id"
            );
            $query->execute(array('user_id' => $user_id));
            $cuisines_arr = $query->fetchALL(PDO::FETCH_OBJ);


            foreach ($cuisines_arr as $db_key=>$cuisine_db){
                foreach ($data as $key=>$cuisine){
                    if($cuisine_db->cuisines_id == $cuisine){
                        unset($cuisines_arr[$db_key]);
                        unset($data[$key]);
                    }
                }
            }// $cuisines_arr  - to delete from db  ,  $data - to new insert
            if($cuisines_arr){
                foreach($cuisines_arr as $key => $cuisine_to_del){
                    $query = $this->pdo->prepare("DELETE FROM cuisines_chefs WHERE cuisines_id = :cuisines_id AND user_id = :user_id");
                    $query->execute(array('cuisines_id' => $cuisine_to_del->cuisines_id, 'user_id' => $user_id));
                }
            }

            foreach ($data as $cuisine){
                    $query = $this->pdo->prepare(
                    "INSERT INTO cuisines_chefs
                        (user_id, cuisines_id)
                     values
                        (:user_id, :cuisines_id)");
                    $data_query = array(
                        "user_id"          => $user_id,
                        "cuisines_id"      => $cuisine,
                    );
                    $query->execute($data_query);
            }
        }
        catch (Exception $e) {
                return $e->getMessage();
                //return $this->cuisines_error;
        }
    }



    public function get_chef_profile($user_id) {

        $couponController = new CouponController;

        $query = $this->pdo->prepare(
            "SELECT DISTINCT *
             FROM chefs
             WHERE user_id = :user_id
             LIMIT 1"
        );
        $query->execute(array('user_id' => $user_id));
        $chef = $query->fetch();

        $return_chef_arr = array('about' => NULL, 'businessInfo' => NULL, 'deliveryInfo' => NULL, 'paymentInfo' => NULL, 'workingTime' => NULL , 'cuisines' => NULL);

        if($chef->about != NULL){
            $return_chef_arr['about'] = $chef->about;
        }
        $return_chef_arr['chefType'] = (int)$chef->chef_type;

        if($chef->business_name != NULL || $chef->business_site != NULL || $chef->business_phone != NULL){
            $return_chef_arr['businessInfo'] = array(
                                                    'name' => $chef->business_name,
                                                    'site' => $chef->business_site,
                                                    'phone'=> $chef->business_phone,
                                                    'registrationId' => $chef->license
                                                    );
        }
        //if($chef->pickup_country != NULL || $chef->pickup_city != NULL || $chef->pickup_street != NULL){
            $return_chef_arr['deliveryInfo'] = array(
                                                    'country'	=> $chef->pickup_country,
                                                    'city'		=> $chef->pickup_city,
                                                    'street'	=> $chef->pickup_street,
                                                    'latitude'	=> $chef->latitude,
                                                    'longitude'	=> $chef->longitude
                                                    );
        //}
        if($chef->stripe_client_id != NULL OR $chef->payment_cash != NULL){
            $return_chef_arr['paymentInfo'] = array(
                                                    'stripeId' 		=> $chef->stripe_client_id,
                                                    'paymentCash' 	=> (bool) $chef->payment_cash
                                                    );
        }


        $return_chef_arr['workingTime'] = $this->working($user_id);
        $return_chef_arr['cuisines'] = $this->get_chef_cuisines($user_id);
        $return_chef_arr['coupon'] = $couponController->get_user_coupon($user_id);


        return $return_chef_arr;

        //    return $this->error_not_uniq_email;
            // return $e->getMessage();
    }

    public function get_chef_cuisines($user_id)
    {
        $array = array();
        $query = $this->pdo->prepare(
            "SELECT * FROM cuisines_chefs
             WHERE user_id = :user_id"
        );
        $query->execute(array('user_id' => $user_id));
        $cuisines_meals = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($cuisines_meals as $key => $value) {
            $array[] = $value->cuisines_id;
        }

        $array = ($array) ? $array : null ;

        return $array;
    }



    private function update_chef_profile($user_id, $data){
// chef_exist
            //check chef
            $query = $this->pdo->prepare(
                "SELECT DISTINCT *
                 FROM chefs
                 WHERE user_id = :user_id
                 LIMIT 1"
            );
            $query->execute(array('user_id' => $user_id));
            $is_chef = $query->fetch();

            $latitude 	= $data['deliveryInfo']->latitude;
        	$longitude 	= $data['deliveryInfo']->longitude;
        	/*
			* check coordinates data , before work with database
        	*/
        	if((!is_numeric($latitude) OR !is_numeric($longitude)) && $latitude != NULL AND $longitude != NULL){
        		return $this->error_coordinates;
        	}
            if((bool)$data['paymentInfo']->paymentCash AND !$latitude AND !$longitude ){
                //if chef not have db coord
                if((int)$is_chef->latitude == 0 AND (int)$is_chef->longitude == 0){
                    return $this->error_chef_addr_required;
                }
            }
            //collect data to db
            $data_query = array(
                        'user_id'   => $user_id
                        );

            if(array_key_exists('about', $data)){
                if($data['about'] !== NULL) {$about = $this->validation->to_string($data['about']);if (!$about)  return $this->error_about;}
                $sql = 'about = :about';
                $data_query['about'] = $data['about'];
            }

            if(array_key_exists('name',$data['businessInfo'])){
                if($data['businessInfo']->name !== NULL) {$business_name = $this->validation->to_string($data['businessInfo']->name);if (!$business_name)  return $this->error_business_name;}
                $sql .= ',business_name = :business_name';
                $data_query['business_name'] = $data['businessInfo']->name;
            }

            if(array_key_exists('site',$data['businessInfo'])){
                if($data['businessInfo']->site !== NULL) {$business_site = $this->validation->to_string($data['businessInfo']->site);if (!$business_site)  return $this->error_business_site;}
                $sql .= ',business_site = :business_site';
                $data_query['business_site'] = $data['businessInfo']->site;
            }

            if(array_key_exists('registrationId',$data['businessInfo'])){
                if($data['businessInfo']->registrationId !== NULL) {$registrationId = $this->validation->to_string($data['businessInfo']->registrationId);if (!$registrationId)  return $this->error_registrationId;}
                $sql .= ',license = :license';
                $data_query['license'] = $data['businessInfo']->registrationId;
            }

            if(array_key_exists('phone',$data['businessInfo'])){
                if($data['businessInfo']->phone !== NULL) {$phone = $this->validation->to_string($data['businessInfo']->phone);if (!$phone)  return $this->error_phone;}
                $sql .= ',business_phone = :business_phone';
                $data_query['business_phone'] = $data['businessInfo']->phone;
            }

            if(array_key_exists('country',$data['deliveryInfo'])){
                if($data['deliveryInfo']->country !== NULL) {$country = $this->validation->to_string($data['deliveryInfo']->country);if (!$country)  return $this->error_country;}else{ $data['deliveryInfo']->country = null;}
                $sql .= ',pickup_country = :pickup_country';
                $data_query['pickup_country'] = $data['deliveryInfo']->country;
            }

            if(array_key_exists('city',$data['deliveryInfo'])){
                if($data['deliveryInfo']->city !== NULL) {$city = $this->validation->to_string($data['deliveryInfo']->city);if (!$city)  return $this->error_city;} else{ $data['deliveryInfo']->city = null;}
                $sql .= ',pickup_city = :pickup_city';
                $data_query['pickup_city'] = $data['deliveryInfo']->city;
            }

            if(array_key_exists('street',$data['deliveryInfo'])){
                if($data['deliveryInfo']->street !== NULL) {$street = $this->validation->to_string($data['deliveryInfo']->street);if (!$street)  return $this->error_street;} else{ $data['deliveryInfo']->street = null;}
                $sql .= ',pickup_street = :pickup_street';
                $data_query['pickup_street'] = $data['deliveryInfo']->street;
            }

            if(array_key_exists('latitude',$data['deliveryInfo'])){
                $sql .= ',latitude = :latitude';
                $data_query['latitude'] = round($data['deliveryInfo']->latitude,5);
            }

            if(array_key_exists('longitude',$data['deliveryInfo'])){
                $sql .= ',longitude = :longitude';
                $data_query['longitude'] = round($data['deliveryInfo']->longitude,5);
            }

            if(array_key_exists('paymentCash',$data['paymentInfo'])){
                if($data['paymentInfo']->paymentCash !== NULL) {$payment_cash = (bool)$data['paymentInfo']->paymentCash;}
                $sql .= ',payment_cash = :payment_cash';
                $data_query['payment_cash'] = $payment_cash;
            }
            try{ // try catch database save
                if( isset($is_chef->user_id) ){ // update chef

                    if ($is_chef->stripe_client_id === NULL AND $payment_cash === FALSE )
                        $this->set_unlisted_meals($is_chef->user_id);

                if($sql[0]==',') {$sql = substr($sql, 1);}	// remove first , from sql query
                $query = $this->pdo->prepare(
                                        "UPDATE chefs
                                         SET $sql
                                         WHERE user_id = :user_id
                                         LIMIT 1"
                                        );


                $result = $query->execute($data_query);

                }else{ // insert chef
                    //check when PUT inserting chef to required
                    if(!$business_name || !$phone){
                        return $this->error_required_chef_profile;
                    }
                    //check when PUT inserting chef

                    $insert = "(";
                    $values = "(";

                    foreach ($data_query as $key=>$value) {
                        $insert .= $key;
                        $values .= ":".$key;
                        if ($key != end(array_keys($data_query))){
                            $insert .= ', ';
                            $values .= ', ';
                        }
                    }
                    $insert .= ")";
                    $values .= ")";
                    $query = $this->pdo->prepare(
                    "INSERT INTO chefs
                        $insert
                     values
                        $values"
                    );

                    $query->execute($data_query);
                }
            } catch (Exception $e) {
            	//return $this->chefprofile_error;
                return $e->getMessage();
            }
    }


    private function set_unlisted_meals($chef_user_id)
    {
        $query = $this->pdo->prepare(
            "UPDATE meals
             SET listed = :listed
             WHERE user_id = :user_id"
        );
        $update = array(
            'user_id'    => $chef_user_id,
            'listed'     => FALSE
        );
        $result = $query->execute($update);
    }



    /**
     * @url POST /comment
     *
     */
    public function comment($data)
    {
        // проверка ордера на комплитед
        $user_id  = $this->check_auth()->user_id;
        $order_id = ( (int) $data->orderId > 0) ? (int) $data->orderId : false;

        $text     = html_entity_decode($this->validation->to_string($data->text), ENT_QUOTES);
        $text     = ($text) ? $text : '';

        $rating   = (int) $data->rate;
        $rating   = ($rating > -1 AND $rating < 6 ) ? $rating : false;

        if ( ! $order_id OR ! $rating)
            return $this->error_invalid_input;

        $query = $this->pdo->prepare(
            "SELECT DISTINCT meal_id, sender_user_id, chef_id
             FROM orders
             WHERE id = :id
             LIMIT 1"
        );

        $params = array(
            "id"      => $order_id
        );

        $query->execute($params);
        $orders = $query->fetch();

        if ( ! $orders OR $orders->chef_id == $user_id)
            return $this->error_order;

        $created  = gmdate("Y-m-d H:i:s");

        $query = $this->pdo->prepare(
            "INSERT INTO comments
                (order_id, meal_id, user_id, text, created, rating)
             values
                (:order_id, :meal_id, :user_id, :text, :created, :rating)"
        );

        $params = array(
            "order_id" => $order_id,
            "meal_id"  => $orders->meal_id,
            "user_id"  => $user_id,
            "text"     => $text,
            "created"  => $created,
            "rating"   => $rating
        );

        try {
            $insert = $query->execute($params);
            $id = $this->pdo->lastInsertId();


            $push = new PushController;
            $push->push_new_comment($order_id, $orders->chef_id, $rating);

            return array(
                'data' => array(
                    'orderId' => (int) $order_id,
                    'mealId'  => (int) $orders->meal_id,
                    'created' => $created,
                    'message' => $text,
                    'rating'  => (int) $rating,
                    'user'    => $this->get_user($user_id)
                )
            );
        } catch (Exception $e) {
            return $this->error_comment_exists;
        }
    }


    /**
     * @url GET /comments/$meal_id
     *
     */
    public function comments($meal_id)
    {
        if ( (int) $meal_id < 1)
            return $this->error_invalid_meal_id;

        $comments = array();

        $query = $this->pdo->prepare(
            "SELECT * FROM comments
             WHERE meal_id = :meal_id"
        );

        $query->execute(array('meal_id' => $meal_id));
        $result = $query->fetchAll(PDO::FETCH_OBJ);

        if ( ! $result)
            $comments = null;

        foreach ($result as $key => $comment) {
            $comments[] = array(
                'orderId' => (int) $comment->order_id,
                'mealId'  => (int) $comment->meal_id,
                'created' => $comment->created,
                'message' => $comment->text,
                'rating'  => (int) $comment->rating,
                'user'    => $this->get_user($comment->user_id)
            );
        }

        return array('data' => $comments);
    }

    /**
     * @url POST /feedback
     *
     */
    public function feedback($data){
        if(!$data->feedBack){
            return $this->error_required_text;
        }
        $user_id = (int) $this->check_auth()->user_id;
        $query = $this->pdo->prepare(
                                        "SELECT DISTINCT email
                                         FROM users
                                         WHERE id = :id
                                         LIMIT 1"
                );
        $query->execute(array('id' => $user_id));
        $user_obj = $query->fetchObject();

        $mailSMTP = new SendMailSmtpClass('noreply.foodridge@gmail.com', 'Y6Jn<eKM4VeAGQt', 'ssl://smtp.gmail.com', 'foodridge');

        $headers  = "MIME-Version: 1.0\r\n";
        $headers  .= "From: Feedback form <$user_obj->email>\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "Reply-To: <$user_obj->email>\r\n";
        $result   = $mailSMTP->send($this->settings['admin_email'], "feedback from $user_obj->email", $data->feedBack , $headers);

        if($result === true){
            return array('data'  => 'ok');
        }else{
            $this->error_send_email['error']['invalid send'] = $result;
            return $this->error_send_email;
        }
    }

    /**
     * @url POST /recommend
     *
     */
    public function recommend($data)
    {   
        if(!$data->name)   {return $this->error_name_required;}
        $email = ($this->validation->is_email($data->email)) ? $data->email : false;
        if ( ! $email) return $this->error_email;        
        

        $user_id = (int) $this->check_auth()->user_id;
        $query = $this->pdo->prepare(
                                        "SELECT DISTINCT name, email
                                         FROM users
                                         WHERE id = :id
                                         LIMIT 1"
                );
        $query->execute(array('id' => $user_id));
        $user_obj = $query->fetchObject();

        $title = "New Chef suggested from no dish listing page";
        $text = "$user_obj->name , $user_obj->email has suggested $data->name , $data->email to register on Foodridge.";

        $mailSMTP = new SendMailSmtpClass('noreply.foodridge@gmail.com', 'Y6Jn<eKM4VeAGQt', 'ssl://smtp.gmail.com', 'foodridge');

        $headers  = "MIME-Version: 1.0\r\n";
        $headers  .= "From: New Chef suggested\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "Reply-To: <$user_obj->email>\r\n";
        $result   = $mailSMTP->send($this->settings['admin_email'], $title, $text , $headers);

        if($result === true){
            return array('data'  => 'ok');
        }else{
            $this->error_send_email['error']['invalid send'] = $result;
            return $this->error_send_email;
        }
    }

    /** Function get users that i'm follow
     *  return array - array of users list that i am follow
     *  @url GET /following
     */
    public function following()
    {   
        $user_id  = $this->check_auth()->user_id;
        
        //get array of users ids
        $query = $this->pdo->prepare(
                                    "SELECT follow_user_id
                                     FROM followers
                                     WHERE user_id = :user_id"
                                    );
        $query->execute(array(
                            'user_id'        => $user_id,
                            ));
        $follow_arr = $query->fetchAll(PDO::FETCH_COLUMN);

        //get info about users i am follow
        foreach ($follow_arr as $follow_uid) {
            $following[] = $this->get_user($follow_uid);
        }
        
        return array('data' => $following);
    }

    /** Fuction get users that follows to specific chef id
    *   return array - array of users list that follow to specific chef
    *   @url GET /followed/$chefId
    **/
    public function followed($chefId)
    {   
        $user_id  = $this->check_auth()->user_id;

        //get array of users ids who follow to chefId
        $query = $this->pdo->prepare(
                                    "SELECT user_id
                                     FROM followers
                                     WHERE follow_user_id = :follow_user_id"
                                    );
        $query->execute(array(
                            'follow_user_id'        => $chefId,
                            ));
        $followed_arr = $query->fetchAll(PDO::FETCH_COLUMN);

        /**
        *  Get arrow of my personal follow , to get users i like
        *  return array - of users that i like  
        */
        $query = $this->pdo->prepare(
                                    "SELECT follow_user_id
                                     FROM followers
                                     WHERE user_id = :user_id"
                                    );
        $query->execute(array(
                            'user_id'        => $user_id,
                            ));
        $follow_arr = $query->fetchAll(PDO::FETCH_COLUMN);
        

        //get info about users that follow to chef_id
        foreach ($followed_arr as $uid) {
            $followed[] = $this->get_user($uid);
        }
        
        foreach ($followed as $key => $followed_user) {
            if(in_array($followed_user['id'], $follow_arr)){
                $current_follow = TRUE;
            }
            else{
                $current_follow = FALSE;
            }
            $followed[$key]['current_follow'] = $current_follow;
        }

        return array('data' => $followed);
    }


    /**
     * user invite method tracking Firebase invite to foodridge server
     * if POST - create invite record , if PUT - update record,  and send users pair to followers table
     * return string - OK
     * @url PUT  /invite
     * @url POST /invite
     *
     */
    public function invite($data)
    {	
        $InviteFirebaseID 	= $this->validation->to_string($data->InviteFirebaseID); // id of invite in firebase system 
    	$inviterID 			= (int)$data->inviterID; // user who send invite
    	$invitedID 			= (int)$data->invitedID; // user who receive invite
    	switch ($_SERVER['REQUEST_METHOD']) 
    	{
    		case 'POST': //create record with unique firebaseInviteID in database
    			if(!$InviteFirebaseID OR !$inviterID){ return $this->errors_required;}
    			$query = $this->pdo->prepare(
						                    "INSERT INTO invite_tokens
						                        (inviter_id, invite_token)
						                     values
						                        (:inviterID, :InviteFirebaseID)"
					                		);
				$data = array(
		                    "inviterID"    		=> $inviterID,
		                    "InviteFirebaseID" 	=> $InviteFirebaseID,
		                	);
                try{
                    $result = $query->execute($data);
                } catch (Exception $e) {
                    return $this->error_firebase_id_exist;
                }

				break;

			case 'PUT': // update invite table and add Invited user id
				if(!$InviteFirebaseID OR !$invitedID){ return $this->errors_required;}
				
                    //Get inviter id from database                
                    $query = $this->pdo->prepare(
                                                "SELECT DISTINCT inviter_id
                                                 FROM invite_tokens
                                                 WHERE invite_token = :invite_token
                                                 LIMIT 1"
                                                );
                    $query->execute(array('invite_token' => $InviteFirebaseID));
                    $inviterID = $query->fetchColumn();


                    $query = $this->pdo->prepare(
                                                "INSERT INTO invites
                                                    (invite_token, inviter_id, invited_id, date)
                                                 values
                                                    (:invite_token, :inviterID, :invitedID , :date)"
                                                );
                    $data = array(
                                "invite_token"  => $InviteFirebaseID,
                                "inviterID"     => $inviterID,
                                "invitedID"     => $invitedID,
                                "date"          => gmdate("Y-m-d H:i:s")
                                );
    		        //send invite push to chef who invited user
    		        $push = new PushController;
    		        
                	$push->push_new_invite_to_chef($InviteFirebaseID, $invitedID);

                    try{
                        $result = $query->execute($data);
                    } catch (Exception $e) {
                        return $this->error_already_follow;
                    }

				break;
		}
		
        return array('data'  => 'ok');
    }

    /**
     * @url PUT /push_period
     *
     */
    public function push_period($data)
    {
        if (!$data->pushFrequency) return $this->error_pushFrequency;   
        
        $user_id  = $this->check_auth()->user_id;

        //update
        $data = array("id" => $user_id, "type_of_daily_push" => $data->pushFrequency);
        $query = $this->pdo->prepare(
                                    "UPDATE users SET last_push_send = NOW(), type_of_daily_push = :type_of_daily_push
                                     WHERE id = :id"
                                    );
        try
        {
            $result = $query->execute($data);
        }catch (Exception $error)
        {
            return $this->db_error;
        }
        return array('data'  => 'ok');
        
    }
}


