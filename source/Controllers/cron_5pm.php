<?php $syspath = preg_replace('/\/api\/source\/Controllers/', '', dirname(__FILE__));
define(SYSPATH, $syspath);
require(SYSPATH.'/api/source/Controllers/Base.php');
require(SYSPATH.'/api/source/Controllers/PushController.php');


class Cron5pm extends Base
{
    function __construct() {
        parent::__construct();
    }


    public function start_5pm()
    {
        //get users who follow to chefs with active coupon
        $query = $this->pdo->prepare(
                                    "SELECT DISTINCT followers.user_id , users.last_push_send ,users.type_of_daily_push
                                     FROM followers
                                     INNER JOIN coupons ON (followers.follow_user_id = coupons.chef_id)
                                     INNER JOIN users ON (followers.user_id = users.id)
                                     WHERE coupons.active_coupon = TRUE
                                     "
                                    );
        
        $query->execute();
        $users_arr = $query->fetchAll(PDO::FETCH_OBJ);

        $curr_date  = new DateTime ();
        $push       = new PushController;

        foreach ($users_arr as $user) {

            $last_send_date = new DateTime ($user->last_push_send);   
            $user_id        = $user->user_id; 
            switch ($user->type_of_daily_push) {
                case(1):
                        //update
                        $this->update_push_user($user_id);
                    break;

                case(2):
                        $difference = $curr_date->diff($last_send_date);

                        if($difference->d == 0 OR $difference->d == 3)
                        {   
                            //update
                            $this->update_push_user($user_id);
                        }
                    break;

                case(3):
                        $difference = $curr_date->diff($last_send_date);
                        if($difference->d == 0 OR $difference->d == 7)
                        {   
                            //update
                            $this->update_push_user($user_id);
                        }
                    break;

            }
            
        }
        
    }

    public function update_push_user($user_id)
    {   
        $push = new PushController;
        //send push here
        $push->push_check_your_deals($user_id);

        $data = array("id" => $user_id);
        $query = $this->pdo->prepare(
                                    "UPDATE users SET last_push_send = NOW()
                                     WHERE id = :id"
                                    );
        $result = $query->execute($data);

        $fd = fopen(SYSPATH . "/api/log/cron-5pm.txt","a");
        fwrite($fd, "log $user_id".date("d.m.Y H:i:s")."\r\n");
        fclose($fd);

    }


    protected function check_auth(){
        return true;
    }
}
$start = new Cron5pm();
$start->start_5pm();

