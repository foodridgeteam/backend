<?php defined('SYSPATH') or die('No direct script access.');

class CouponController extends Base
{
    function __construct() {
        parent::__construct();
    }

    /**
     * Coupon routing method , GET , POST , PUT, DELETE requests
     * @url GET /coupon/$data
     * @url PUT /coupon
     * @url POST /coupon
     * @url DELETE /coupon/$data 
     */
    public function coupon($data) {

        $coupon_id = $data;

        //handler of http requests
        switch ($_SERVER['REQUEST_METHOD']) 
        {
            // result - id coupon , or error exception
            case 'POST':
                 $result = $this->insert_coupon($data);
            break;

            // result - id coupon , or error exception
            case 'PUT':
                 $result = $this->update_coupon($data);
            break;

            // delete coupon if no active orders . return data ok
            case 'DELETE':
                 $result = $this->delete_coupon($coupon_id);

                 return array(
                                'data' => 'ok'
                             );
            break;

            // set coupon id as param to get_coupon() method
            default:
                $result = $coupon_id;
                
        }

        if(is_array($result)){
            if($result['error'] != NULL)
            {
                return $result;
            }
        }
        

        $arr = $this->get_coupon($result);
        
        return array(
            'data' => $arr
        );        

    }

    /**
     * insert new coupon , after insert record - trigger in coupons table
     * input array -  data array to insert to db
     * return integer - id of inserted coupon
     */
    private function insert_coupon($data)
    {   
        //validate data
        if(!$data->dateFrom || !$data->dateTo || !$data->discount || !$data->couponName)
        {
            return $this->error_required_coupon_entity;
        }

        //prepare data
        $chefId         = (int) $this->check_auth()->user_id;
        $dateFrom       = date('Y-m-d', strtotime($data->dateFrom));
        $dateTo         = date('Y-m-d', strtotime($data->dateTo));
        $discount       = (int)$data->discount;
        $coupon_name    = $this->validation->to_string($data->couponName);
        $active_coupon  = (bool)$data->activeCoupon;
        
        // check call to determ realy active coupon by dates
        if($active_coupon == TRUE){
            $valid = $this->check_coupon_valid_by_date($dateFrom , $dateTo);
            //if not valid coupon return error
            
            if($valid == false){
                return $this->error_not_valid_dates;
            }
        }

        //check call to maximum value discount
        if($data->discount > 80){ return $this->error_maximum_discount;}

        //temporary check, check for unique coupon for chef, return error if not unique 
        $query = $this->pdo->prepare("SELECT id FROM coupons WHERE chef_id = :chef_id");
        $query->execute(array(':chef_id' => $chefId));
        $exist_coupon = $query->fetchColumn();

        if($exist_coupon != false)
        {
            return $this->error_coupon_limit;
        }

        // array of data to db
        $data = array(
                        "chef_id"           => $chefId,
                        "coupon_name"       => $coupon_name,
                        "date_from"         => $dateFrom,
                        "date_to"           => $dateTo,
                        "discount"          => $discount,
                        "active_coupon"     => $active_coupon 
                    );

        

        //db insert query
        $query = $this->pdo->prepare(
            "INSERT INTO coupons
                (chef_id, date_from , date_to , discount, coupon_name, active_coupon)
             values
                (:chef_id, :date_from , :date_to , :discount, :coupon_name , :active_coupon)"
            );
        try
        {
            $result = $query->execute($data);
            $id = $this->pdo->lastInsertId();
        }catch (Exception $error)
        {
            return $this->db_error;
        }
        //send invite push to chef who invited user
        //$push = new PushController;
        
        //$push->push_new_coupon_to_chef($chefId);


        return $id;
    }

    /**
     * update info about coupon
     * input  array , integer  - array of data values , couponId
     * return integer - id of updated coupon
     */
    private function update_coupon($data)
    {   
        //validate data
        if(!$data->couponId){ return array("error" => array("code" => 40, "message" => "required params : couponId"));}
        if(!$data->dateFrom || !$data->dateTo || !$data->discount || !$data->couponName || !$data->couponId)
        {
            return $this->error_required_coupon_entity;
        }

        //prepare data
        $chefId         = (int) $this->check_auth()->user_id;

        $coupon_id      = (int)$data->couponId;
        $dateFrom       = date('Y-m-d', strtotime($data->dateFrom));
        $dateTo         = date('Y-m-d', strtotime($data->dateTo));
        $discount       = (int)$data->discount;
        $coupon_name    = $this->validation->to_string($data->couponName);
        $active_coupon  = (bool)$data->activeCoupon;


        // check call to determ realy active coupon by dates
        if($active_coupon == TRUE){
            $valid = $this->check_coupon_valid_by_date($dateFrom , $dateTo);

            //if not valid coupon return error
            if($valid == false){
                return $this->error_not_valid_dates;
            }
        }
        //check call to maximum value discount
        if($data->discount > 80){ return $this->error_maximum_discount;}

        // array of data to db
        $data = array(
                        "id"            => $coupon_id,
                        "coupon_name"   => $coupon_name,
                        "date_from"     => $dateFrom,
                        "date_to"       => $dateTo,
                        "discount"      => $discount,
                        "active_coupon" => $active_coupon 
                    );

        //db query
        $query = $this->pdo->prepare(
                                    "UPDATE coupons SET coupon_name = :coupon_name, date_from = :date_from , date_to = :date_to , discount = :discount, active_coupon = :active_coupon 
                                     WHERE id = :id"
                                    );
        try{
            $result = $query->execute($data);
        }catch (Exception $error){
            return $this->db_error;
        }
        return $coupon_id;
        
    }

    /**
     * method to get single existing coupon by coupon ID
     * return array - single coupon array
     *
     */
    private function get_coupon($id)
    {   
        
        //db query
        $query = $this->pdo->prepare(
                    "SELECT *
                     FROM coupons
                     WHERE id = :id
                     LIMIT 1"
                );
        $query->execute(array('id' => $id));
        $coupon_obj = $query->fetch(PDO::FETCH_OBJ);

        //configure return function output 
        if($coupon_obj)
        {
            $couponArr = array(
                            "id"                => (int)$coupon_obj->id,            
                            "couponName"        => $coupon_obj->coupon_name,            
                            "dateFrom"          => $coupon_obj->date_from,            
                            "dateTo"            => $coupon_obj->date_to,            
                            "discount"          => (int)$coupon_obj->discount,
                            "activeCoupon"      => (bool)$coupon_obj->active_coupon,
                            "usedCounter"       => (int)$coupon_obj->used_number ,
                            "daysLeft"          => $this->count_days_left($coupon_obj->date_to)         
                            );

            return $couponArr;
        }

    }

    /**
     * method to remove coupon by coupon ID, if user have not opened orders
     * input [int] - (id of coupon to remove)
     * return void
     */
    private function delete_coupon($id)
    {   
        //db query
        $query = $this->pdo->prepare("DELETE FROM coupons WHERE id = :id");
        try{
            $query->execute(array('id' => $id));
        }catch (Exception $error){
            return $this->db_error;
        }
        
    }


    /**
     * get coupons list by chef ID 
     * input integer - chefId
     * return array - coupons list array ,  NULL - if no coupons
     */
    public function get_user_coupon($chef_id)
    {   
        //db query
        $query = $this->pdo->prepare(
                    "SELECT *
                     FROM coupons
                     WHERE chef_id = :chef_id
                     LIMIT 1"
                );
        $query->execute(array(':chef_id' => $chef_id));
        $coupon_obj = $query->fetch(PDO::FETCH_OBJ);

        if($coupon_obj){
            //configure coupons list output array
            $couponArr[] = array(
                            "id"                => (int)$coupon_obj->id,            
                            "couponName"        => $coupon_obj->coupon_name,            
                            "dateFrom"          => $coupon_obj->date_from,            
                            "dateTo"            => $coupon_obj->date_to,            
                            "discount"          => (int)$coupon_obj->discount,
                            "activeCoupon"      => (bool)$coupon_obj->active_coupon,
                            "usedCounter"       => (int)$coupon_obj->used_number,
                            "daysLeft"          => $this->count_days_left($coupon_obj->date_to)                   
                            );

            return $couponArr;    
        } 
        
    }

    /**
     * check if coupon valid or not 
     * input [string , string] , [from date to check , to date to check]
     * return bool - flag of coupon validity
     */
    private function check_coupon_valid_by_date($date_from , $date_to)
    {   
        //default value
        $coupon_available = false;

        $current_gm_date = gmdate("Y-m-d H:i:s");
        if($current_gm_date > $date_from  AND $current_gm_date < $date_to )
        {
            $coupon_available = true;
        }

        return $coupon_available;
    }


    /**
     * Get discount method
     * @url GET /discount
     */
    public function discount($meal_id) {
        
        $query = $this->pdo->prepare(
                                    "SELECT meals.portion_price , coupons.discount , coupons.active_coupon
                                     FROM meals
                                     INNER JOIN coupons ON (meals.user_id = coupons.chef_id)
                                     WHERE meals.id = :meal_id
                                     AND coupons.date_from  <= NOW()
                                     AND coupons.date_to    > NOW()
                                     AND coupons.active_coupon = TRUE
                                     "
                                    );
        
        $query->execute(array(
                            'meal_id'        => $meal_id,
                            ));
        $mealObj = $query->fetch(PDO::FETCH_OBJ);

        if($mealObj){
            $portion_price  = $mealObj->portion_price;
            $discount       = $mealObj->discount;

            $discount_money_value = $portion_price * $discount / 100;

            $new_price_no_discount = $portion_price + $discount_money_value/2;

            $new_price_with_discount = $new_price_no_discount - $discount_money_value;
            
            return array(
                        "new_price_no_discount"     => $new_price_no_discount,
                        "new_price_with_discount"   => $new_price_with_discount
                        );
        }
        return null;
    
    }


    /**
     * Get user coupon deals request
     * @url GET /deals
     */
    public function deals() {
        $user_id  = $this->check_auth()->user_id;

        $userController = new UserController;
        $chef_ids = $this->get_chefs_active_coupon();

        $latitude_get      =  $_GET['latitude'];
        $longitude_get     =  $_GET['longitude'];
        if(!$_GET['latitude'] OR !$_GET['longitude'])
        {
            $query = $this->pdo->prepare(
                                    "SELECT latitude, longitude
                                     FROM delivery
                                     WHERE delivery.user_id  = $user_id
                                     "
                                    );
            $query->execute();
            $delivery = $query->fetch(PDO::FETCH_OBJ);
            $latitude_get = $delivery->latitude;   
            $longitude_get = $delivery->longitude;   
        }
        
        $list_chefs = $userController->construct_chefs_full($latitude_get, $longitude_get, $chef_ids);
        
        foreach ($list_chefs as $key => $chef) {
            //if(!(int)$chef->latitude OR !(int)$chef->longitude){continue;}
            $full_avatar_path = NULL;
            if($chef->avatar != NULL){
                $full_avatar_path = METHOD."://".$_SERVER['HTTP_HOST'].'/api/img/avatar/medium/'.$chef->avatar;
            }
            $temp = array(
                //'distance'        => $chef->distance,
                'id'            => (int) $chef->user_id,
                'avatar'        => $full_avatar_path,
                'name'          => $chef->name,
                'about'         => $chef->about,
                'rating'        => (float) $chef->rating,
                'votesCount'    => (int) $chef->votes,
                'followers'     => (int) $chef->followers,
                'lastVisit'     => $chef->last_visit,
                'deliveryInfo'  => array(
                    'country'       => $chef->pickup_country,
                    'city'          => $chef->pickup_city,
                    'street'        => $chef->pickup_street,
                    'distance'      => $chef->distance
                ),
                'businessInfo'  => array(
                    'name'              => $chef->business_name,
                    'registrationId'    => $chef->license,
                    'site'              => $chef->business_site,
                    'phone'             => $chef->business_phone
                ),
                'workingTime'   => $userController->working( (int) $chef->user_id),
                'cuisines'      => $userController->get_chef_cuisines( (int) $chef->user_id),
                'follow'        => $userController->get_follow( (int) $chef->user_id),
                'coupon'        => $this->get_user_coupon((int) $chef->user_id)
            );
            if($chef->distance != NULL){
                $temp['deliveryInfo']['latitude']  = $chef->latitude;
                $temp['deliveryInfo']['longitude'] = $chef->longitude;
            }
            if ($search) {
                if (stripos($chef->name, $search) !== FALSE ) {
                    $array[] = $temp;
                }
            } else {
                $array[] = $temp;
            }
        }



        return array('data' => $array);
    }

    /**
     * get array of users ids with active coupons
     * 
     */
    public function following_ids(){
        $user_id  = $this->check_auth()->user_id;

        $current_gm_date = gmdate("Y-m-d H:i:s");

        $query = $this->pdo->prepare(
                                    "SELECT followers.follow_user_id
                                     FROM followers
                                     INNER JOIN coupons ON (followers.follow_user_id = coupons.chef_id)
                                     WHERE followers.user_id = :user_id
                                     AND coupons.date_from  <= NOW()
                                     AND coupons.date_to    > NOW()
                                     AND coupons.active_coupon = TRUE
                                     "
                                    );
        
        $query->execute(array(
                            'user_id'        => $user_id,
                            ));
        $follow_arr = $query->fetchAll(PDO::FETCH_COLUMN);

        return $follow_arr;
    }

    /**
    *  Get chefs with active coupons
    */
    public function get_chefs_active_coupon(){
        $user_id  = $this->check_auth()->user_id;
        $query = $this->pdo->prepare(
                                    "SELECT chef_id
                                     FROM coupons
                                     WHERE coupons.date_from  <= NOW()
                                     AND coupons.date_to    > NOW()
                                     AND coupons.active_coupon = TRUE
                                     AND chef_id <> :user_id
                                     "
                                    );
        
        $query->execute(array(
                            'user_id'        => $user_id,
                            ));
        $chef_ids_arr = $query->fetchAll(PDO::FETCH_COLUMN);

        return $chef_ids_arr;
    }

    public function count_days_left($date_to){
        $now = time(); 
        $your_date = strtotime("$date_to + 1 DAY");
        $datediff = $your_date - $now;

        if($datediff >= 1){
            return floor($datediff / (60 * 60 * 24));
        }
        return 0;
        
    }


}