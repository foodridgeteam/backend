<?php defined('SYSPATH') or die('No direct script access.');

class MealController extends Base
{
    private $list_meals_chef = null;
    private $list_meals_full = null;

	function __construct() {
    	parent::__construct();

        $chef_id = ($_GET['chefId']) ? (int) $_GET['chefId'] : false;

        if ($chef_id)
            if ($this->list_meals_chef == null){
                $latitude   = round($_GET['latitude'], 5);
                $longitude  = round($_GET['longitude'], 5);
                $this->list_meals_chef = $this->construct_meals_chef($chef_id, $latitude , $longitude);
            }

        if ( ! $chef_id AND $_GET['limit']){
            if ($this->list_meals_full == null){
                $latitude   = round($_GET['latitude'], 5);
                $longitude  = round($_GET['longitude'], 5);
                $this->list_meals_full = $this->construct_meals_full($latitude , $longitude);
            }
        }
    }


    private function construct_meals_chef($chef_id, $latitude , $longitude)
    {
        $user_id = (int) $this->check_auth()->user_id;

        $sql = "SELECT meals.id , meals.user_id, meals.meal_name, meals.description, meals.portions_available, meals.portions_sold, meals.rating_meal, meals.votes_meal, meals.spicy_rank, meals.other, meals.portion_price, meals.delivery_price, meals.delivery_type, meals.listed, meals.send_push, chefs.latitude, chefs.longitude ";

        /*
        * if have by distance query
        */
        if($latitude != NULL AND $longitude != NULL AND $chef_id != $user_id)
        {
        $sql .= ",( 6371 * acos( cos( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( latitude ) ) ) ) AS distance ";
        
        }
        $sql .= "FROM meals
        INNER JOIN chefs ON (meals.user_id = chefs.user_id) ";
        
        if ($chef_id != $user_id) {
            $sql .= " WHERE meals.listed = 1 AND meals.user_id = :user_id ";
        }else{
            $sql .= " WHERE meals.user_id = :user_id ";
        }
        /*
        *  if order by distance , or not
        */
        if($latitude != NULL AND $longitude != NULL AND $chef_id != $user_id)
        {
            $sql .= "HAVING distance < ".DEFAULTRADIUS."
                     ORDER BY distance ASC ";
        }else
        {
            $sql .= "ORDER BY id DESC";
        }

        $query = $this->pdo->prepare("$sql");
        $query->execute(array('user_id' => $chef_id));
        $meals = $query->fetchAll(PDO::FETCH_OBJ);

        return $meals;
    }


    private function construct_meals_full($latitude , $longitude)
    {
        
        $sql = "SELECT meals.id , meals.user_id, meals.meal_name, meals.description, meals.portions_available, meals.portions_sold, meals.rating_meal, meals.votes_meal, meals.spicy_rank, meals.other, meals.portion_price, meals.delivery_price, meals.delivery_type, meals.listed, meals.send_push, chefs.latitude, chefs.longitude ";
        
        /*
        * if have by distance query
        */
        if($latitude != NULL AND $longitude != NULL)
        {
        $sql .= ",( 6371 * acos( cos( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( latitude ) ) ) ) AS distance ";
        
        }
        $sql .= "FROM meals
        INNER JOIN chefs ON (meals.user_id = chefs.user_id) WHERE meals.listed = 1 ";
        /*
        *  if order by distance , or not
        */
        if($latitude != NULL AND $longitude != NULL)
        {
            $sql .= "HAVING distance < ".DEFAULTRADIUS."
                     ORDER BY distance ASC ";
        }else
        {
            $sql .= "ORDER BY id DESC";
        }
        


        $query = $this->pdo->prepare($sql);
        $query->execute();
        $meals = $query->fetchAll(PDO::FETCH_OBJ);
        
        return $meals;
    }

    private function get_meals()
    {
        $array   = array();
        $limit   = (int) $_GET['limit'];
        $offset  = (int) $_GET['offset'];
        $search  = ($_GET['search']) ? $this->validation->to_string($_GET['search']) : false;

        if ($_GET['chefId'])
            $list_meals = $this->list_meals_chef;

        if ( ! $_GET['chefId'] AND $_GET['limit'])
            $list_meals = $this->list_meals_full;

        $userController     = new UserController;
        $couponController   = new CouponController;

        foreach ($list_meals as $key => $meal) {
            $temp = array(
                'id'                => (int) $meal->id,
                'distance'          => $meal->distance,
                'chef'              => $userController->get_chef($meal->user_id)['data'],
                'name'              => $meal->meal_name,
                'description'       => $meal->description,
                'cuisines'          => $this->get_cuisines_meal($meal->id),
                'ingredients'       => $this->get_meal_recipe($meal->id),
                'photo'             => $this->get_meal_fotos($meal->id),
                'portionsAvailable' => (int) $meal->portions_available,
                'portionsSold'      => (int) $meal->portions_sold,
                'rating'            => (float) $meal->rating_meal,
                'votesCount'        => (int) $meal->votes_meal,
                'spicyRank'         => (int) $meal->spicy_rank,
                'flags'             => $this->get_meal_flag($meal->id),
                'other'             => $meal->other,
                'portionPrice'      => (float) $meal->portion_price,
                'deliveryPrice'     => (float) $meal->delivery_price,
                'listed'            => (bool) $meal->listed,
                'deliveryType'      => (int) $meal->delivery_type,
                'discount'          => $couponController->discount((int)$meal->id) 
            );
            if($meal->distance == NULL){
                unset($temp['chef']['deliveryInfo']['latitude']);
                unset($temp['chef']['deliveryInfo']['longitude']);
            }

            if ($search) {
                if (stripos($meal->meal_name, $search) !== FALSE ) {
                    $array[] = $temp;
                }
            } else {
                $array[] = $temp;
            }
        }
        //var_dump($array);
        $array_offset = array_chunk($array, $limit);

        return array('data' => $array_offset[$offset]);
    }


    private function create_meal($data)
    {
        $user_id = (int) $this->check_auth()->user_id;

        //check user role
		$query = $this->pdo->prepare("SELECT role_id FROM users WHERE id = :id");
        $query->execute(array(':id' => $user_id));
        $user_role = $query->fetchColumn();
        if($user_role == 2){
        	return $this->role_error;
        }
        //required fields
		$required_meals_arr = array("name","portionsAvailable","portionPrice","deliveryPrice");

        $data_arr = (array) $data;
        foreach ($required_meals_arr as $meal) {
        	if (!array_key_exists($meal, $data_arr)){
        		$required_params .= $meal." ";
        	}
        }
        if($required_params){
        	return array("error" => array("code" => 40, "message" => "required params : $required_params"));
        }

        //types
		$meal_name   			= $this->validation->to_string($data->name); if (!$meal_name) return $this->error_meal_name;
		$portions_available   	= (int)$data->portionsAvailable;
		$spicy_rank   			= ($data->spicyRank)     ? (int)$data->spicyRank : 0;
		$portion_price   		= number_format($data->portionPrice, 2, '.', '');
		$delivery_price   		= number_format($data->deliveryPrice, 2, '.', '');
		$delivery_type   		= ($data->deliveryType)  ? (int)$data->deliveryType : 1;
		$listed   				= ($data->listed)        ? (boolean)$data->listed : 0;
		if($data->description) 	{$description = html_entity_decode($this->validation->to_string($data->description),ENT_QUOTES);  if (!$description) return $this->error_description;}
		if($data->other) 		{$other = $this->validation->to_string($data->other); if (!$other) return $this->error_other;}



		$query = $this->pdo->prepare(
            "INSERT INTO meals
                (user_id,  meal_name, portions_available , spicy_rank , portion_price , delivery_price , delivery_type, listed, description, other)
             values
                (:user_id, :meal_name, :portions_available , :spicy_rank , :portion_price , :delivery_price , :delivery_type, :listed, :description , :other)"
            );

		$data_query = array(
            "user_id"       		=> $user_id,
            "meal_name"     		=> $meal_name,
            "portions_available" 	=> $portions_available,
            "spicy_rank"     		=> $spicy_rank,
            "portion_price"     	=> $portion_price,
            "delivery_price"     	=> $delivery_price,
            "delivery_type"     	=> $delivery_type,
            "listed"     			=> $listed,
            "description"			=> $description,
            "other"					=> $other
        );
        try{
        $result = $query->execute($data_query);
        $meal_id = $this->pdo->lastInsertId();

        } catch (Exception $e) {
                return $this->db_error;
        }

        // push isset listed
        if ($listed === true) {
            $push = new PushController;
            $push->push_to_followers($meal_id);
        }


        //insert cousines
        if($data->cuisines){
            $cuisines = array_unique($data->cuisines);
            try{
            foreach ($cuisines as $cuisine){
                $query = $this->pdo->prepare(
                "INSERT INTO cuisines_meals
                    (meal_id, cuisines_id)
                 values
                    (:meal_id, :cuisines_id)");
                $data_query = array(
                    "meal_id"          => $meal_id,
                    "cuisines_id"      => $cuisine,
                );
                $query->execute($data_query);
            }
            }catch (Exception $e) {
                return $this->error_cuisines;
            }
        }
        //insert meal flags
        if($data->ingredients){
            $ingredients = array_unique($data->ingredients);
            try{
            foreach ($ingredients as $ingredient){
                $query = $this->pdo->prepare(
                "INSERT INTO meal_recipe
                    (meal_id, ingredient_id)
                 values
                    (:meal_id, :ingredient_id)");
                $data_query = array(
                    "meal_id"       => $meal_id,
                    "ingredient_id" => $ingredient,
                );
                $query->execute($data_query);
            }
            }catch (Exception $e) {
                return $this->error_ingredients;
            }
        }
        //insert cousine flags

        $meal_data = $data->flags;

        $query = $this->pdo->prepare(
            "INSERT INTO meal_flags
                (meal_id, breakfast, lunch, dinner, bakery, vegetarian, egg, glutenFree, tracesNuts , garlic , ginger)
             values
                (:meal_id, :breakfast, :lunch, :dinner, :bakery, :vegetarian, :egg, :glutenFree, :tracesNuts , :garlic , :ginger)");
            $data_query = array(
                "meal_id"       => $meal_id,
                "breakfast"     => (boolean)$meal_data->breakfast,
                "lunch"         => (boolean)$meal_data->lunch,
                "dinner"        => (boolean)$meal_data->dinner,
                "bakery"        => (boolean)$meal_data->bakery,
                "vegetarian"    => (boolean)$meal_data->vegetarian,
                "egg"           => (boolean)$meal_data->egg,
                "glutenFree"   => (boolean)$meal_data->glutenFree,
                "tracesNuts"   => (boolean)$meal_data->tracesNuts,
                "garlic"        => (boolean)$meal_data->garlic,
                "ginger"        => (boolean)$meal_data->ginger
            );
            try{
            $query->execute($data_query);
            } catch (Exception $e) {
                return $this->db_error;
            }

        return $this->single_meal($meal_id);
    }


    //update entity meal
    private function update_meal($data){
        $data = (array) $data;
        $meal_id = $data['mealId'];
        $user_id = (int) $this->check_auth()->user_id;
        // check if listed true chef must have stripe
        //get stripe client id
/*        $query = $this->pdo->prepare(
            "SELECT stripe_client_id
             FROM chefs
             WHERE user_id = :user_id"
        );
        $query->execute(array('user_id' => $user_id));
        $stripe_client_id = $query->fetchColumn();*/



        $query = $this->pdo->prepare(
            "SELECT stripe_client_id, payment_cash
             FROM chefs
             WHERE user_id = :user_id"
        );
        $query->execute(array('user_id' => $user_id));
        $payment_type = $query->fetch();


        //check meal id
        $query = $this->pdo->prepare(
            "SELECT id
             FROM meals
             WHERE id = :id"
        );
        $query->execute(array('id' => $meal_id));
        $meal_id = $query->fetchColumn();
        if($meal_id == NULL){
            return $this->error_mealId_exist;
        }
        /*if($stripe_client_id == NULL AND (boolean)$data['listed'] == TRUE){
            return $this->error_list_without_stripe; // listed true and NO stripe return error
        }*/
        if($payment_type->stripe_client_id == NULL AND (bool) $payment_type->payment_cash == FALSE AND (boolean)$data['listed'] == TRUE){
            return $this->error_list_without_stripe; // listed true and NO stripe return error
        }
        // check if listed true chef must have stripe

        if(array_key_exists('name', $data)){
                if($data['name'] !== NULL) {$name = $this->validation->to_string($data['name']);if (!$name)  return $this->error_meal_name;}
                $sql = 'meal_name = :meal_name';
                $data_query['meal_name'] = $name;
        }
        if(array_key_exists('description', $data)){
                if($data['description'] !== NULL) {$description = $this->validation->to_string($data['description']);if (!$description)  return $this->error_description;}
                $sql .= ',description = :description';
                $data_query['description'] = $description;
        }
        if(array_key_exists('portionsAvailable', $data)){
                if($data['portionsAvailable'] !== NULL) {$portions_available = (int)$data['portionsAvailable'];}
                $sql .= ',portions_available = :portions_available';
                $data_query['portions_available'] = $portions_available;
        }
        if(array_key_exists('portionPrice', $data)){
                if($data['portionPrice'] !== NULL) {$portion_price = number_format($data['portionPrice'], 2, '.', '');}
                $sql .= ',portion_price = :portion_price';
                $data_query['portion_price'] = $portion_price;
        }
        if(array_key_exists('deliveryPrice', $data)){
                if($data['deliveryPrice'] !== NULL) {$delivery_price = number_format($data['deliveryPrice'],2, '.', '');}
                $sql .= ',delivery_price = :delivery_price';
                $data_query['delivery_price'] = $delivery_price;
        }
        if(array_key_exists('spicyRank', $data)){
                if($data['spicyRank'] !== NULL) {$spicy_rank = (int)$data['spicyRank'];}
                $sql .= ',spicy_rank = :spicy_rank';
                $data_query['spicy_rank'] = $spicy_rank;
        }
        if(array_key_exists('deliveryType', $data)){
                if($data['deliveryType'] !== NULL) {$delivery_type = (int)$data['deliveryType'];}
                $sql .= ',delivery_type = :delivery_type';
                $data_query['delivery_type'] = $delivery_type;
        }
        if(array_key_exists('listed', $data)){
                if($data['listed'] !== NULL) {$listed = (boolean)$data['listed'];}
                $sql .= ',listed = :listed';
                $data_query['listed'] = $listed;
        }
        if(array_key_exists('other', $data)){
                if($data['other'] !== NULL) {$other = $this->validation->to_string($data['other']);if (!$other)  return $this->error_other;}
                $sql .= ',other = :other';
                $data_query['other'] = $other;
        }
        $data_query['meal_id'] = $data['mealId'];
        if($sql[0]==',') {$sql = substr($sql, 1);}
        if($sql){
            $query = $this->pdo->prepare(
                                            "UPDATE meals
                                             SET $sql
                                             WHERE id = :meal_id
                                             LIMIT 1"
                                            );

            try{
            $result = $query->execute($data_query);
            // push isset listed
            if ($listed === true) {
                $push = new PushController;
                $push->push_to_followers( (int) $data['mealId']);
            }

            } catch (Exception $e) {
                return $this->db_error;
            }
        }
        //update cousines
        if(array_key_exists('cuisines', $data)){
            try{
                $query = $this->pdo->prepare(
                    "SELECT cuisines_id
                     FROM cuisines_meals
                     WHERE meal_id = :meal_id"
                );
                $query->execute(array('meal_id' => $meal_id));
                $cuisines_arr = $query->fetchALL(PDO::FETCH_OBJ);
                $cuisines_put = $data['cuisines'];

                foreach ($cuisines_arr as $db_key=>$cuisine_db){
                    foreach ($cuisines_put as $key => $cuisine){
                        if($cuisine_db->cuisines_id == $cuisine){
                            unset($cuisines_arr[$db_key]);
                            unset($cuisines_put[$key]);
                        }
                    }
                }
                // $cuisines_arr  - to delete from db  ,  $cuisines_put - to new insert
                if($cuisines_arr){
                    foreach($cuisines_arr as $key => $cuisine_to_del){
                        $query = $this->pdo->prepare("DELETE FROM cuisines_meals WHERE cuisines_id = :cuisines_id AND meal_id = :meal_id");
                        $query->execute(array('cuisines_id' => $cuisine_to_del->cuisines_id, 'meal_id' => $meal_id));
                    }
                }

                foreach ($cuisines_put as $cuisine){
                        $query = $this->pdo->prepare(
                        "INSERT INTO cuisines_meals
                            (meal_id, cuisines_id)
                         values
                            (:meal_id, :cuisines_id)");
                        $data_query = array(
                            "meal_id"          => $meal_id,
                            "cuisines_id"      => $cuisine
                        );
                        $query->execute($data_query);
                }
            }
            catch (Exception $e) {
                    //return $e->getMessage();
                    return $this->cuisines_error;
            }
        }
        // update ingridients
        if(array_key_exists('ingredients', $data)){
            try{
                $query = $this->pdo->prepare(
                    "SELECT ingredient_id
                     FROM meal_recipe
                     WHERE meal_id = :meal_id"
                );
                $query->execute(array('meal_id' => $meal_id));
                $ingredients_arr = $query->fetchALL(PDO::FETCH_OBJ);

                $ingredients_put = $data['ingredients'];


                foreach ($ingredients_arr as $db_key=>$ingredient_db){
                    foreach ($ingredients_put as $key => $ingredient){
                        if($ingredient_db->ingredient_id == $ingredient){
                            unset($ingredients_arr[$db_key]);
                            unset($ingredients_put[$key]);
                        }
                    }
                }

                // $ingridisents_arr  - to delete from db  ,  $ingridisents_put - to new insert
                if($ingredients_arr){
                    foreach($ingredients_arr as $key => $ingredient_to_del){
                        $query = $this->pdo->prepare("DELETE FROM meal_recipe WHERE ingredient_id = :ingredient_id AND meal_id = :meal_id");
                        $query->execute(array('ingredient_id' => $ingredient_to_del->ingredient_id, 'meal_id' => $meal_id));
                    }
                }
                foreach ($ingredients_put as $ingredient){
                        $query = $this->pdo->prepare(
                        "INSERT INTO meal_recipe
                            (meal_id, ingredient_id)
                         values
                            (:meal_id, :ingredient_id)");
                        $data_query = array(
                            "meal_id"          => $meal_id,
                            "ingredient_id"    => $ingredient
                        );
                        $query->execute($data_query);
                }
            }
            catch (Exception $e) {
                    //return $e->getMessage();
                    return $this->ingredient_error;
            }
        }
        //update meals flags
        if(array_key_exists('flags', $data)){
            $meal_data = $data['flags'];
            $query = $this->pdo->prepare(
                    "UPDATE meal_flags
                     SET    breakfast = :breakfast, lunch = :lunch, dinner = :dinner, bakery = :bakery, vegetarian = :vegetarian, egg = :egg, glutenFree = :glutenFree, tracesNuts = :tracesNuts, garlic = :garlic, ginger = :ginger
                     WHERE  meal_id   = :meal_id
                     LIMIT 1"
                );

                $data_query = array(
                    "meal_id"       => $meal_id,
                    "breakfast"     => (boolean)$meal_data->breakfast,
                    "lunch"         => (boolean)$meal_data->lunch,
                    "dinner"        => (boolean)$meal_data->dinner,
                    "bakery"        => (boolean)$meal_data->bakery,
                    "vegetarian"    => (boolean)$meal_data->vegetarian,
                    "egg"           => (boolean)$meal_data->egg,
                    "glutenFree"   => (boolean)$meal_data->glutenFree,
                    "tracesNuts"   => (boolean)$meal_data->tracesNuts,
                    "garlic"        => (boolean)$meal_data->garlic,
                    "ginger"        => (boolean)$meal_data->ginger
                );
                try{
                $query->execute($data_query);
                } catch (Exception $e) {
                return $this->db_error;
                }
        }
        return $this->single_meal($meal_id);
    }

    private function get_cuisines_meal($meal_id)
    {
        $array = array();
        $query = $this->pdo->prepare(
            "SELECT * FROM cuisines_meals
             WHERE meal_id = :meal_id"
        );
        $query->execute(array('meal_id' => $meal_id));
        $cuisines_meals = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($cuisines_meals as $key => $value) {
            $array[] = $value->cuisines_id;
        }

        $array = ($array) ? $array : null ;

        return $array;
    }


    private function get_meal_recipe($meal_id)
    {
        $array = array();
        $query = $this->pdo->prepare(
            "SELECT * FROM meal_recipe
             WHERE meal_id = :meal_id"
        );
        $query->execute(array('meal_id' => $meal_id));
        $meal_recipe = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($meal_recipe as $key => $value) {
            $array[] = $value->ingredient_id;
        }

        $array = ($array) ? $array : null ;

        return $array;
    }


    private function get_meal_fotos($meal_id)
    {
        $array = array();
        $query = $this->pdo->prepare(
            "SELECT * FROM meal_fotos
             WHERE meal_id = :meal_id
             ORDER BY id ASC"
        );
        $query->execute(array('meal_id' => $meal_id));
        $meal_fotos = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($meal_fotos as $key => $value) {
            $array[] = METHOD."://".$_SERVER['HTTP_HOST'].'/api/img/meals/medium/'.$value->foto;
        }

        $array = ($array) ? $array : null ;

        return $array;
    }


    private function get_meal_flag($meal_id)
    {
        $array = array();
        $query = $this->pdo->prepare(
            "SELECT * FROM meal_flags
             WHERE meal_id = :meal_id"
        );
        $query->execute(array('meal_id' => $meal_id));
        $meal_flags = $query->fetchAll(PDO::FETCH_ASSOC);
        $meal_flags = $meal_flags[0];
        array_shift($meal_flags);

        foreach ($meal_flags as $key => $value) {
            $array[$key] = (bool) $value;
        }

        return $array;
    }

    /**
     * @url GET /single_meal/$id
     */
    public function single_meal($id)
    {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT * FROM meals
             WHERE id = :id
             LIMIT 1"
        );

        $query->execute(array('id' => $id));
        $meal = $query->fetch();

        $userController     = new UserController;
        $couponController   = new CouponController;

        return array(
            'data' => array(
                'id'                => (int) $meal->id,
                'chef'              => $userController->get_chef($meal->user_id)['data'],
                'name'              => $meal->meal_name,
                'description'       => $meal->description,
                'cuisines'          => $this->get_cuisines_meal($meal->id),
                'ingredients'       => $this->get_meal_recipe($meal->id),
                'photo'             => $this->get_meal_fotos($meal->id),
                'portionsAvailable' => (int) $meal->portions_available,
                'portionsSold'      => (int) $meal->portions_sold,
                'rating'            => (float) $meal->rating_meal,
                'votesCount'        => (int) $meal->votes_meal,
                'spicyRank'         => (int) $meal->spicy_rank,
                'flags'             => $this->get_meal_flag($meal->id),
                'other'             => $meal->other,
                'portionPrice'      => (float) $meal->portion_price,
                'deliveryPrice'     => (float) $meal->delivery_price,
                'listed'            => (bool) $meal->listed,
                'deliveryType'      => (int) $meal->delivery_type,
                'discount'          => $couponController->discount((int)$meal->id) 
                
            )
        );
    }





    /**
     * @url GET /meal
     * @url POST /meal
     * @url PUT /meal
     */
    public function meal($data)
    {
        $user_id = (int) $this->check_auth()->user_id;

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                return $this->get_meals();
                break;
            case 'POST':
                return $this->create_meal($data);
                break;
            case 'PUT':
                if(!$data->mealId){
                    return $this->error_requered_meal;
                }
                return $this->update_meal($data);
                break;
        }
    }





    /**
     *
     * @url GET /cuisines
     *
     */
    public function cuisines()
    {
        $array = array();
        $query = $this->pdo->prepare(
            "SELECT * FROM cuisines"
        );
        $query->execute();
        $cuisines = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($cuisines as $key => $value) {
            $array[] = array(
                'id'    => (int) $value->id,
                'name'  => $value->cuisines_name
            );
        }

        return array(
            'data' => $array
        );
    }


    /**
     *
     * @url GET /ingredients
     *
     */
    public function ingredients()
    {
        $array = array();
        $query = $this->pdo->prepare(
            "SELECT * FROM ingredients"
        );
        $query->execute();
        $ingredients = $query->fetchAll(PDO::FETCH_OBJ);

        foreach ($ingredients as $key => $value) {
            $array[] = array(
                'id'    => (int) $value->id,
                'name'  => $value->ingredient_name
            );
        }

        return array(
            'data' => $array
        );
    }


}
