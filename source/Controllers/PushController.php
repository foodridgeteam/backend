<?php $syspath = preg_replace('/\/api\/source\/Controllers/', '', dirname(__FILE__));
define(SYSPATH, $syspath);

class PushController extends Base
{
    private $api_key = 'AIzaSyDMxk6XAi5eTgeyuXvZb4ox6IYijP_HUR0';
    private $cert;
    private $array_check_push_id_ios = array();

    private $type_new_meal    = 101;
    private $type_new_order   = 102;
    private $type_new_message = 103;
    private $type_new_comment = 104;
    private $type_new_invite_user   = 105;
    private $type_new_coupon        = 106;
    private $type_new_deal          = 107;

    function __construct() {
        parent::__construct();

        $this->cert = SYSPATH . '/api/source/certificate/pushSert.pem';
    }


    protected function check_auth(){
        return true;
    }


    private function create_log_push($result = null)
    {
        $fd = fopen(SYSPATH . "/api/log/push.txt","a");
        fwrite($fd, date("d.m.Y H:i:s"). ", request = ". $result . "\r\n");
        fclose($fd);
    }


    private function get_status_name($id)
    {
        if($id == 6){
            return 'paid';
        }
        $query = $this->pdo->prepare(
            "SELECT DISTINCT name_status
             FROM order_statuses
             WHERE id = :id
             LIMIT 1"
        );
        $query->execute(array('id' => $id));
        $name_status = $query->fetchColumn();

        return $name_status;
    }


    private function get_push($user_id)
    {
        $this->array_clean_feedback();

        $query = $this->pdo->prepare(
            "SELECT push_id, push_flag, device_type
             FROM sessions
             WHERE user_id = :user_id
             AND push_id IS NOT NULL"
        );

        $query->execute(array(
            "user_id" => $user_id
        ));
        $array_push = $query->fetchAll(PDO::FETCH_OBJ);

        if ( ! $array_push) {
            $this->create_log_push('no push_id in DB');
            return;
        }

        return $array_push;
    }


    private function get_url($push_flag = null)
    {

        switch ($push_flag) {
            case '1':
                $url = 'ssl://gateway.sandbox.push.apple.com:2195';
                break;

            case '2':
                $url = 'ssl://gateway.push.apple.com:2195';
                break;

            case 'feedback_1':
                $url = 'ssl://feedback.sandbox.push.apple.com:2196';
                break;

            case 'feedback_2':
                $url = 'ssl://feedback.push.apple.com:2196';
                break;

            default:
                $url = 'https://gcm-http.googleapis.com/gcm/send';
                break;
        }

        return $url;
    }


    private function to_android($push_id, $message, $entity_id, $type)
    {
        $header = array(
            'Content-Type: application/json',
            'Authorization: key=' . $this->api_key
        );

        $data_string = json_encode(array(
            'to'   => $push_id,
            'data' => array(
                'message'  => $message,
                'type'     => $type,
                'entity'   => $entity_id
            )
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->get_url());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $result = curl_exec($ch);

        return $result;
    }


    private function to_ios($push_id, $message, $entity_id, $type, $push_flag, $type_payment = NULL)
    {
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $this->cert);
        $fp = stream_socket_client($this->get_url($push_flag), $err, $errstr, 80, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if ( ! $fp) {
            $this->create_log_push("IOS Failed to connect: $err $errstr ,"  . $data_string . ", string cycle № $i----" . PHP_EOL);
            return false;
        }

        $data_arr = array(
            'aps'      => array(
                'alert'  => $message,
                'sound'  => 'default'
            ),
            'type'     => $type,
            'entity'   => $entity_id,
        );
        if($type_payment != NULL){
    		$data_arr['paymentType'] = (int)$type_payment;
        }

		$data_string = json_encode($data_arr);

        $msg = chr(0) . pack('n', 32) . pack('H*', $push_id) . pack('n', strlen($data_string)) . $data_string;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);

        return $result;
    }


    private function send_feedback($push_flag)
    {
        $array = array();
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $this->cert);
        $fp = stream_socket_client($this->get_url($push_flag), $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
        if ( ! $fp) return;

        while( ! feof($fp)) {
            $data = fread($fp, 38);
            if(strlen($data)) {
                $array[] = unpack("N1timestamp/n1length/H*devtoken", $data);
            }
        }
        fclose($fp);

        return $array;
    }


    private function clean_user_session($push_id, $result, $ios = null)
    {
        $obj = json_decode($result);

        if ($obj->results[0]->error == 'NotRegistered' OR $ios === true) {
            $query = $this->pdo->prepare(
                "DELETE FROM sessions WHERE push_id = :push_id"
            );
            $query->execute(array('push_id' => $push_id));
        }
    }


    private function array_clean_feedback()
    {
        $feedback_push_id = array();
        $push_flag_feedback = array(
            'feedback_1',
            'feedback_2'
        );

        foreach ($push_flag_feedback as $flag) {
            array_unique(array_merge($feedback_push_id, $this->send_feedback($flag)));
        }

        foreach ($feedback_push_id as $key => $push_id) {
            $this->clean_user_session($push_id, null, true);
        }
    }


    /**
     *
     * @url POST /send_push
     *
     */
    public function send_push($user_id, $message, $entity_id, $type , $type_payment = NULL)
    {
        $array_push  = $this->get_push($user_id);
        $i = 0;

        foreach ($array_push as $push) {
            $i++;

            switch ($push->device_type) {
                case '1':
                    $result = $this->to_android($push->push_id, $message, $entity_id, $type);
                    break;

                case '2':
                    $result = $this->to_ios($push->push_id, $message, $entity_id, $type, $push->push_flag, $type_payment);
                    break;
            }

            if ( ! $result) {
                $this->create_log_push("Message not delivered, result $result, string cycle № $i----" . PHP_EOL);
                continue;
            }

            $this->create_log_push("Message successfully delivered, result $result, string cycle № $i----" . PHP_EOL);
            $this->clean_user_session($push->push_id, $result);
        }
    }



    /**
     *
     * @url GET /send_test
     *
     */
    public function send_test()
    {
        self::push_new_comment(3, 1, 'test message api apns and entityId');
    }



    public function push_to_followers($meal_id)
    {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT meals.meal_name,
             meals.user_id,
             users.name
             FROM meals
             INNER JOIN users ON (users.id = meals.user_id)
             WHERE meals.id = :id
             AND listed = 1
             AND send_push = 0
             LIMIT 1"
        );
        $query->execute(array('id' => $meal_id));
        $obj = $query->fetch();

        if ( ! $obj) return;

        $query = $this->pdo->prepare(
            "UPDATE meals
             SET send_push = 1
             WHERE id = :id
             LIMIT 1"
        );
        $query->execute(array("id" => $meal_id));

        $message = "Chef " . $obj->name . " added new meal " . $obj->meal_name . "";

        $query = $this->pdo->prepare(
            "SELECT user_id
             FROM followers
             WHERE follow_user_id = :follow_user_id"
        );
        $query->execute(array(
            "follow_user_id" => $obj->user_id
        ));
        $users = $query->fetchAll(PDO::FETCH_OBJ);

        if ( ! $users) return;

        foreach ($users as $user) {
            self::send_push($user->user_id, $message, $meal_id, $this->type_new_meal);
        }
    }



    public function push_change_status_order($order_id, $status_id)
    {
        $name_status = $this->get_status_name($status_id);

        $query = $this->pdo->prepare(
            "SELECT DISTINCT sender_user_id, chef_id, delivery_type, type_payment
             FROM orders
             WHERE id = :id
             LIMIT 1"
        );
        $query->execute(array('id' => $order_id));
        $user = $query->fetch();

        if ( ! $user) return;


        if ($user->delivery_type == 2)
            $message = "Pick up Order #" . $order_id. " was " . $name_status;
        else
            $message = "Delivery Order #" . $order_id. " was " . $name_status;


        switch ($status_id) {
            case '5':
                self::send_push($user->chef_id, $message, $order_id, $status_id, $user->type_payment);
                break;

            case '7':
                self::send_push($user->chef_id, $message, $order_id, $status_id, $user->type_payment);
                break;

            case '9':
                self::send_push($user->sender_user_id, $message, $order_id,  $status_id, $user->type_payment);
                self::send_push($user->chef_id, $message, $order_id, $status_id, $user->type_payment);
                break;

            default:
                self::send_push($user->sender_user_id, $message, $order_id,  $status_id, $user->type_payment);
                break;
        }
    }



    public function push_to_chef_new_order($chef_id, $order_id, $total_price, $delivery_type, $type_payment = NULL)
    {
        //  2 - pickup, 3 - delivery

        if ($delivery_type == 2)
            $message = "New Pick up Order #" . $order_id. ", Amount $" . $total_price;
        else
            $message = "New Delivery Order #" . $order_id. ", Amount $" . $total_price;

        self::send_push($chef_id, $message, $order_id, $this->type_new_order , $type_payment);
    }



    public function push_make_payment($transaktion_id, $status_id)
    {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT id, sender_user_id, chef_id, delivery_type
             FROM orders
             WHERE transaktion_id = :transaktion_id
             LIMIT 1"
        );
        $query->execute(array('transaktion_id' => $transaktion_id));
        $order = $query->fetch();

        if ( ! $order) return;


        if ($order->delivery_type == 2)
            $message = "Pick up Order #" . $order->id  . ", Payment Successful";
        else
            $message = "Delivery Order #" . $order->id  . ", Payment Successful";

        self::send_push($order->chef_id, $message, $order->id, $status_id);
    }



    public function push_set_cron($order_id, $status_id, $sender_user_id, $chef_id, $delivery_type)
    {
        if ($delivery_type == 2)
            $message = "Pick up Order #" . $order_id. "  was cancelled";
        else
            $message = "Delivery Order #" . $order_id. "  was cancelled";

        self::send_push($sender_user_id, $message, $order_id, $status_id);
        self::send_push($chef_id, $message, $order_id, $status_id);
    }


    public function push_new_message_user($order_id, $user_id, $message)
    {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT sender_user_id, chef_id
             FROM orders
             WHERE id = :id
             LIMIT 1"
        );
        $query->execute(array('id' => $order_id));
        $order = $query->fetch();

        if ( ! $order) return;


        $message = "New message to Order #" . $order_id . " : " .$message;

        $to_user_id = ($user_id == $order->sender_user_id) ? $order->chef_id : $order->sender_user_id;

        self::send_push($to_user_id, $message, $order_id, $this->type_new_message);
    }



    public function push_new_comment($order_id, $user_id, $rating)
    {
        $message = "New comment to Order #" . $order_id . ", rating: $rating";

        self::send_push($user_id, $message, $order_id, $this->type_new_comment);
    }

    /**
    *   PUSH function to send notification to chef when someone takes firebase invite from this chef 
    *
    */
    public function push_new_invite_to_chef($InviteFirebaseID, $user_id)
    {   
        //get inviter id
        $query = $this->pdo->prepare(
            "SELECT DISTINCT inviter_id
             FROM invites
             WHERE invite_token = :InviteFirebaseID
             LIMIT 1"
        );
        $query->execute(array('InviteFirebaseID' => $InviteFirebaseID));
        $inviteObj = $query->fetchObject();

        //get info name
        $query = $this->pdo->prepare(
            "SELECT DISTINCT name
             FROM users
             WHERE id = :user_id
             LIMIT 1"
        );
        $query->execute(array('user_id' => $user_id));
        $invitedObj = $query->fetchObject();

        $message = "$invitedObj->name has accepted your invitation";

        //send push to user who invite, with data about user who was invited
        self::send_push($inviteObj->inviter_id, $message, $user_id, $this->type_new_invite_user);
    }

    /**
    *   PUSH function to send notification to chef when he publish coupon 
    *
    */
    /*public function push_new_coupon_to_chef($user_id)
    { 
        $message = "Your deals are now published";

        //send push to chef who publish coupon 
        self::send_push($user_id, $message, $user_id, $this->type_new_coupon);

    }*/

    /**
    *   PUSH function to send notification of check deals by cron 
    *
    */
    public function push_check_your_deals($user_id)
    { 
        $message = "Check today's Special Dinner Deals";

        //send push to chef who publish coupon 
        self::send_push($user_id, $message, $user_id, $this->type_new_deal);

    }

}