<?php
class ForgotController extends Base
{
    function __construct() {
        parent::__construct();

    }


    protected function check_auth(){
        return true;
    }



    private function delete_forgot($user_id = null)
    {
        $query = $this->pdo->prepare(
            "DELETE FROM forgot
             WHERE user_id = :user_id"
        );
        $query->execute(array('user_id' => $user_id));
    }



    private function check_forgot($user_id, $date)
    {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT * FROM forgot
             WHERE user_id = :user_id
             AND date = :date LIMIT 1"
        );
        $params = array(
            "user_id" => $user_id,
            "date"    => $date
        );
        $query->execute($params);
        $forgot = $query->fetch();

        return $forgot;
    }



    private function update_user_password($user_id = null, $email = null, $pass = null)
    {
        $query = $this->pdo->prepare(
            "UPDATE users
             SET password = :password
             WHERE id = :id AND email = :email"
        );
        $data_query = array(
            'id'       => $user_id,
            'email'    => $email,
            'password' => $pass
        );
        $query->execute($data_query);
    }



    private function decode_url($url)
    {
        $iv_size        = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv             = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $url_param_dec  = rawurldecode($url);
        $in_crypt_param = base64_decode($url_param_dec);
        $iv_dec         = substr($in_crypt_param, 0, $iv_size);
        $in_crypt_param = substr($in_crypt_param, $iv_size);
        $key            = pack('H*', SALT);
        $in_param       = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $in_crypt_param, MCRYPT_MODE_CBC, $iv_dec);
        $in_param       = rtrim($in_param, "\0");
        $in_param       = json_decode($in_param);

        return $in_param;
    }



    private function block_account($param)
    {
        $param = rawurlencode($param);

        $in_param = $this->decode_url($param);

        $forgot = $this->check_forgot($in_param->user_id, $in_param->date);

        if ($forgot->hash !== $param)
            return 'invalid link';

        $this->update_user_password($in_param->user_id, $in_param->email);

        $this->delete_forgot($in_param->user_id);

        return 'Account block!';
    }


    private function form_change_password($param)
    {
        $param = rawurlencode($param);

        echo '<!DOCTYPE html>
                <html lang="en">
                  <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>Forgot password Foodridge</title>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
                    <!--[if lt IE 9]>
                      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                    <![endif]-->
                  </head>
                  <body>
                    <div class="container" style="padding-top:30px;">
                        <div class="row">
                            <form action="/api/forgot" method="post">
                              <div class="form-group">
                                <label for="new_pass">Input new password</label>
                                <input id="new_pass" type="password" class="form-control" name="pass" placeholder="Password" value="" required>
                                <input id="hidden" type="hidden" name="hidden"  value="' . $param . '">
                              </div>
                              <button id="change" type="submit" class="btn btn-default">Change password</button>
                            </form>
                        </div>
                    </div>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
                  </body>
                </html>';

    }



    private function save_change_password()
    {
        if ($_SERVER['HTTP_REFERER'] !== 'http://' . $_SERVER['HTTP_HOST'] . '/api/forgot?param=' . $_POST['hidden'])
            exit;

        if ( ! $_POST['pass'])
            exit;

        // 12 часов возможность на восстановление, потом линк не действительный
        $current_date   = gmdate('Y-m-d H:i:s');
        $stamp          = strtotime($current_date) - 12*60*60;
        $old_date       = gmdate('Y-m-d H:i:s', $stamp);

        $in_param = $this->decode_url($_POST['hidden']);

        $user_id        = $in_param->user_id;

        $forgot = $this->check_forgot($user_id, $in_param->date);


        if ($forgot->hash !== $_POST['hidden'])
            return 'invalid link';

        if ($in_param->date < $old_date) {
            $this->delete_forgot($user_id);

            return 'invalid link';
        }

        $password = hash('sha512', md5(SALT.md5($_POST['pass'])));

        $this->update_user_password($user_id, $in_param->email, $password);

        $this->delete_forgot($user_id);

        return 'Password changed !';
    }






    /**
     * @url GET /forgot
     * @url POST /forgot
     */
    public function forgot($data)
    {
        if ($_GET['param'])
            return $this->form_change_password($_GET['param']);

        if ($_GET['block'])
            return $this->block_account($_GET['block']);

        if ($_POST['hidden'])
            return $this->save_change_password();


        $email = ($this->validation->is_email($data->email)) ? $data->email : false;
        if ( ! $email) return $this->error_email;

        $query = $this->pdo->prepare(
            "SELECT DISTINCT id FROM users
             WHERE email = :email LIMIT 1"
        );

        $query->execute(array('email' => $email));
        $user_id = $query->fetchColumn();

        if ( ! $user_id)
            return $this->error_email;

        $date = gmdate('Y-m-d H:i:s');

        $param = array(
            "user_id" => (int) $user_id,
            "email"   => $email,
            "date"    => $date
        );
        $param = json_encode($param);

        $iv_size            = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv                 = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $key                = pack('H*', SALT);
        $crypt_param        = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $param, MCRYPT_MODE_CBC, $iv);
        $crypt_param        = $iv . $crypt_param;
        $crypt_param_base64 = base64_encode($crypt_param);
        $url_param          = rawurlencode($crypt_param_base64);

        $link = METHOD."://" . $_SERVER['HTTP_HOST'] . "/api/forgot?param=$url_param";
        $link_block = METHOD."://" . $_SERVER['HTTP_HOST'] . "/api/forgot?block=$url_param";

        $query = $this->pdo->prepare(
            "INSERT INTO forgot
                (user_id, hash, date)
             values
                (:user_id, :hash, :date)"
        );
        $params = array(
            "user_id" => $user_id,
            "hash"    => $url_param,
            "date"    => $date
        );

        $this->delete_forgot($user_id);

        try {
            $insert = $query->execute($params);
        } catch (Exception $e) {
            return $this->error_send_email;
        }



        $mailSMTP = new SendMailSmtpClass('noreply.foodridge@gmail.com', 'Y6Jn<eKM4VeAGQt', 'ssl://smtp.gmail.com', 'foodridge');

        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: foodridge <foodridge-noreply@gmail.com>\r\n";
        $result   = $mailSMTP->send($email, 'Forgot password from Foodridge', "Click on the link and change your <a href='$link'>password</a> from Foodridge. \r\n \r\n \r\nIf you are not registered, you can close your account  <a href='$link_block'>block account</a>", $headers);

        if($result === true){
            return array('data'  => 'ok');
        }else{
            $this->error_send_email['error']['invalid send'] = $result;
            return $this->error_send_email;
        }
    }
}