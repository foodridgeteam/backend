<?php defined('SYSPATH') or die('No direct script access.');

class OrderController extends Base
{
    function __construct() {
    	parent::__construct();
	}

	/**
     * order
     * @url GET /order/$data
     * @url PUT /order
     * @url POST /order
     *
     */
    public function order($data) {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                if(!$data->dishId || !$data->quantity || !$data->deliveryType || !$data->totalSum){
                    return $this->error_create_order_params;
                }
                $result = $this->create_order($data);
                break;
            case 'PUT':
                if(!$data->orderId || !$data->orderStatus){
                    return $this->error_update_order_params;
                }
                $result = $this->update_status($data);
                break;
            case 'GET':
                if(!$data){
                    return $this->error_required_order_id;
                }
                $result = $this->return_order($data);
        }
        return $result;
    }

    private function create_order($data){

       $sender_id       = (int) $this->check_auth()->user_id;
       $meal_id         = (int) $data->dishId;
       $quantity        = (int) $data->quantity;
       $delivery_type   = (int) $data->deliveryType;
       $stripeToken     = $data->stripeToken;
       $type_payment     = (int) $data->typePayment;

       $discount_payment = $data->discountPayment; // NULL - no discont avail , TRUE - discount avail and used , FALSE - discount avail and not used

       $query = $this->pdo->prepare(
            "SELECT meals.user_id, meals.portion_price, meals.portions_available, meals.delivery_price, meals.delivery_type, coupons.discount , coupons.active_coupon , coupons.id AS coupon_id , chefs.stripe_client_id , chefs.payment_cash  FROM meals
             INNER JOIN chefs ON (chefs.user_id = meals.user_id)
             LEFT JOIN coupons ON (meals.user_id = coupons.chef_id)
             WHERE meals.id = :id
             LIMIT 1"
        );
        $query->execute(array('id' => $meal_id));
        
        $meal_obj = $query->fetch(PDO::FETCH_OBJ);

        
        //get and check user delivery address
        $query = $this->pdo->prepare(
            "SELECT  delivery.country, delivery.city, delivery.street, users.phone  FROM users
             LEFT JOIN delivery ON (delivery.user_id = users.id)
             WHERE users.id = :user_id
             LIMIT 1"
        );
        $query->execute(array('user_id' => $sender_id));
        $sender_obj = $query->fetch(PDO::FETCH_OBJ);

        if($discount_payment != NULL AND !$meal_obj->active_coupon)
        {
            return $this->error_not_active_coupon;    
        }
        
        //check discount part
        if($discount_payment !== NULL)
        {   
            $portion_price  = $meal_obj->portion_price;
            $discount       = $meal_obj->discount;

            $discount_money_value = $portion_price * $discount / 100;
            $new_price_no_discount = $portion_price + $discount_money_value/2;
            $new_price_with_discount = $new_price_no_discount - $discount_money_value;

            if($discount_payment == TRUE)// TRUE - discount avail and used
            {
                $data->portion_price = $new_price_with_discount; // price single portion

                $total_price = $new_price_with_discount * $quantity; // count total sum of order
                //if delyvery + delivery to sum
                if((int)$delivery_type != 2){
                    $total_price = $total_price + $meal_obj->delivery_price;
                }
                if(round($data->totalSum,2) != round($total_price,2)){
                    return $this->error_with_discount_mismatch;
                }
                $data->coupon_id        = $meal_obj->coupon_id;
                $data->coupon_value     = $discount;
                
            }
            else // FALSE - discount avail and not used
            {   
                $data->portion_price = $new_price_no_discount; // price single portion

                $total_price = $new_price_no_discount * $quantity; // count total sum of order
                //if delyvery + delivery to sum
                if((int)$delivery_type != 2){
                    $total_price = $total_price + $meal_obj->delivery_price;
                }

                if(round($data->totalSum,2) != round($total_price,2)){
                    return $this->error_with_no_discount_mismatch;
                }
                
            }

        }else{
            $data->portion_price = $meal_obj->portion_price; // price single portion
            $total_price = $meal_obj->portion_price * $quantity; // count total sum of order
            //if delyvery + delivery to sum
            if((int)$delivery_type != 2){
                $total_price = $total_price + $meal_obj->delivery_price;
            }
            if(round($data->totalSum,2) != round($total_price,2)){
                return $this->error_total_price;
            }
        }

        //error if user has no phone
        if($sender_obj->phone == NULL){
            return $this->error_required_phone;
        }
        // if no type payment return error
        if($type_payment == NULL){
            return $this->error_required_type_payment;
        }

        // error if user not have delivery address but have delivery to home
        if(($sender_obj->country == NULL || $sender_obj->city == NULL || $sender_obj->street == NULL) AND ((int)$delivery_type == 3 OR (int)$delivery_type == 1)){
            return $this->error_fill_delivery;
        }
        
        // check to right delivery type
        if(($delivery_type == 2 AND $meal_obj->delivery_type == 3) || ($delivery_type == 3 AND $meal_obj->delivery_type == 2)){
            return $this->error_delivery_mismatch;
        }

        //check available portions if 0 - return error
        if($meal_obj->portions_available < $quantity){
            return $this->error_portions_number;
        }

        //check chef exit stripe application account

        // if stripe payment
        if($type_payment == 1)
        {
            try
            {
                \Stripe\Stripe::setApiKey(STRIPECLIENTSECRET);
                $res = \Stripe\Account::retrieve($meal_obj->stripe_client_id);
            }catch (Exception $e) 
            {
                // delete stripe client id if chef delete application
                $query = $this->pdo->prepare(
                            "UPDATE chefs
                             SET    stripe_client_id = :stripe_client_id
                             WHERE  user_id   = :user_id
                             LIMIT 1"
                        );
                $data_query = array(
                            "user_id"       => $receiver_id,
                            "stripe_client_id" => NULL
                        );
                $query->execute($data_query);

                // set all chef meals to listed 0 , if chef had only stripe payment and disconnect it
                if($meal_obj->payment_cash == 0){
                    $query = $this->pdo->prepare(
                            "UPDATE meals
                             SET    listed = :listed
                             WHERE  user_id   = :user_id"
                        );
                    $data_query = array(
                                "user_id"       => $receiver_id,
                                "listed"        => 0
                            );
                    $query->execute($data_query);
                }
                
                return $this->error_chef_exist_stripe;
            }

            $order = $this->pre_order($data);
            
    /*
    *  if payment type cash - make cash order
    */
    }else if($type_payment == 2)
    {
            $order = $this->insert_order($data);
    }
        /*
        *  if errors return error. or return order json
        */
        if (array_key_exists('error',$order)){
            return $order;     
        }
        return $this->return_order($order);
    }

    /*
    *  Insert order to database , both types cash and stripe charge
    */
    protected function insert_order($data , $transaktion_id = NULL)
    {   
        $sender_id       = (int) $this->check_auth()->user_id;
        $meal_id         = (int) $data->dishId;
        $quantity        = (int) $data->quantity;
        $delivery_type   = (int) $data->deliveryType;
        $type_payment    = (int) $data->typePayment;

        if($data->latitude != NULL AND $data->longitude != NULL)
        {
            $latitude        = round($data->latitude, 5);
            $longitude       = round($data->longitude, 5);
        }else{
            /*
            *  if delivery in order and no coords - try get coords from user profile
            */
            if($delivery_type == 3){
                $coordinates = $this->get_userprofile_coordinates($sender_id);
                
                if(!$coordinates->latitude or !$coordinates->longitude){return $this->error_delivery_required;}
                $latitude  = $coordinates->latitude;
                $longitude = $coordinates->longitude;
            }
        }


        $query = $this->pdo->prepare(
            "SELECT chefs.stripe_client_id, meals.meal_name, meals.user_id
             FROM meals
             INNER JOIN chefs ON (meals.user_id = chefs.user_id)
             WHERE meals.id = :id
             LIMIT 1"
        );
        $query->execute(array('id' => $meal_id));
        $meal_obj = $query->fetchObject();
        $receiver_id = $meal_obj->user_id;
        
        $query = $this->pdo->prepare(
                                        "INSERT INTO orders
                                            (meal_id, total_price, portion_price, quantity , sender_user_id, chef_id, status , created , delivery_type, transaktion_id , type_payment, latitude, longitude, discount, coupon_id, coupon_value)
                                         values
                                            (:meal_id, :total_price, :portion_price, :quantity , :sender_user_id, :chef_id, :status , :created , :delivery_type, :transaktion_id , :type_payment, :latitude, :longitude, :discount, :coupon_id, :coupon_value)"
                                        );

        $data = array(
            "meal_id"           => $meal_id,
            "total_price"       => $data->totalSum,
            "portion_price"     => $data->portion_price,
            "quantity"          => $quantity,
            "sender_user_id"    => $sender_id,
            "chef_id"           => $receiver_id,
            "status"            => 1,
            "created"           => gmdate("Y-m-d H:i:s"),
            "delivery_type"     => $delivery_type,
            "transaktion_id"    => $transaktion_id,
            "type_payment"      => $type_payment,
            "latitude"          => $latitude,
            "longitude"         => $longitude,
            "discount"          => $data->discountPayment,
            "coupon_id"         => $data->coupon_id,
            "coupon_value"      => $data->coupon_value
        );
        $result     = $query->execute($data);
        $order_id   = $this->pdo->lastInsertId();

        $push = new PushController;
        $push->push_to_chef_new_order($receiver_id, $order_id, $totalSum, $delivery_type, $type_payment);

        return $order_id;
    }

    /*
    *   make payment charge and creating order if payment type stripe
    */
    protected function pre_order($data)
    {
        $meal_id         = (int) $data->dishId;
        $stripeToken     = $data->stripeToken;

        if (!$data->stripeToken){  // validate for required params
            return $this->error_invalid_input;
        }
        try 
        {   
            $query = $this->pdo->prepare(
                "SELECT chefs.stripe_client_id, meals.meal_name, meals.user_id, meals.portion_price
                 FROM meals
                 INNER JOIN chefs ON (meals.user_id = chefs.user_id)
                 WHERE meals.id = :id
                 LIMIT 1"
            );
            $query->execute(array('id' => $meal_id));
            $meal_obj = $query->fetchObject();

            /*
            *  make stripe charge here
            */
            $amount                 = $data->totalSum*100;
            $application_fee_int    = (int)$meal_obj->portion_price; //10% application fee, of first price
            $application_fee_int    = (int)$data->quantity * $application_fee_int;
            $application_fee        = ($application_fee_int * 100/10) + 33;
            $stripe_account         = $meal_obj->stripe_client_id;
            
            // Create the charge on Stripe's servers - this will charge the user's card

            \Stripe\Stripe::setApiKey(STRIPECLIENTSECRET);
            $charge = \Stripe\Charge::create(array(
                                                  'amount' => $amount, // amount in cents
                                                  'currency' => 'aud',
                                                  'source' => $stripeToken,
                                                  'capture' => false,
                                                  'destination' => array(
                                                        "amount" => $amount - $application_fee,
                                                        "account" => $stripe_account,
                                                    )
                                                ));
            if($charge['status']=='succeeded')
            {
                $transaktion_id = $charge['id'];
                try{
                    $order_id = $this->insert_order($data , $transaktion_id);
                }catch (Exception $e) {
                    return $this->error_order_create;
                    //return $e->getMessage();
                }

            }
        }catch (Exception $e) {
            $error = $e->getMessage();
            $fd = fopen(SYSPATH . "/api/log/error_pay.txt","a");
            fwrite($fd, date("d.m.Y H:i:s"). ", error = ". $error . "\r\n");
            fclose($fd);
            return $this->error_payment;
            
        }
        return $order_id;
    }


    private function update_status($data){
            /*
            1 created
            2 accepted
            3 prepared
            4 sent
            5 received
            6 payed
            7 user cancel
            8 chef cancel
            9 system cancel
            10 completed
            */
            $white_list  = array(2,3,4,5,7,8);
            $closed_status_list = array(6,7,8,9,10);
            $orderStatus = $data->orderStatus;
            $orderId     = $data->orderId;
            $user_id   = (int) $this->check_auth()->user_id;
            
            //get charge
            $query = $this->pdo->prepare(
							            "SELECT DISTINCT transaktion_id,status, sender_user_id, chef_id , type_payment
							             FROM orders
							             WHERE id = :id
							             LIMIT 1"
		        );
		    $query->execute(array('id' => $orderId));
		    $charge_obj = $query->fetchObject();
            $charge = $charge_obj->transaktion_id;

            /*
            *  if cash payment , we allow to set up status 6 - payed
            */
            if($charge_obj->type_payment == 2){
                array_push($white_list , 6);
                array_shift($closed_status_list);
            }

            if(!in_array($orderStatus,$white_list))
            {
                return $this->error_invalid_status;
            }
            
            if($user_id != $charge_obj->sender_user_id AND $user_id != $charge_obj->chef_id)
            {
                return $this->error_no_permission;
            }
            //get current order status and you can't change closed order
            $current_order_status = $charge_obj->status;
            if(in_array($current_order_status,$closed_status_list)){
                return $this->error_order_closed;
            }

            if($orderStatus == 7 OR $orderStatus == 8){ // if user cancel or chef cancel , make refund
                if($charge_obj->type_payment == 1){
                    $refund_obj = new RefundController;
                    $result = $refund_obj->stripe_make_refund($charge, $orderId);

                    if ($result['error'] != NULL)
                        return $result;
                /*
                * if cash - close order without api stripe
                */
                }else{
                    $query = $this->pdo->prepare(
                        "UPDATE orders
                         SET    payed = :payed
                         WHERE  id    = :id
                         LIMIT 1"
                    );
                    $data_query = array(
                                "id"        => $orderId,
                                "payed"     => 0
                            );
                    $query->execute($data_query);
                }
                
            /*
            *  make real payment if chef send meal and type payment stripe only
            */
            }else if($orderStatus == 4 and $charge_obj->type_payment == 1){ 
            	$result = $this->make_payment($charge);

            	if ($result['error'] != NULL)
	                return $result;
            }
            /*
            * set query string
            */
            $set_string = "status = :status";

            if($orderStatus == 6)
            {
                $set_string .= ", payed = :payed";

            }
            if($orderStatus == 5)
            {
                $set_string .= ", delivered = :delivered";

            }

            /*
            *  update status part
            */
            $query = $this->pdo->prepare(
                                        "UPDATE orders
                                         SET    $set_string
                                         WHERE  id   = :id
                                         LIMIT 1"
                                        );
            $data_query = array(
                                "id"       => $orderId,
                                "status"   => $orderStatus
                                );
            if($orderStatus == 6)
            {
                $data_query["payed"] = 1;
            }
            if($orderStatus == 5)
            {
                $data_query["delivered"] = 1;
            }
            $query->execute($data_query);

            $push = new PushController;
            $push->push_change_status_order($orderId, $orderStatus);

            $this->check_to_complete_order($orderId);

            return $this->return_order($orderId);
    }

    //method check delivery and payed . and make order complete if both
    private function check_to_complete_order($order_id){
        $query = $this->pdo->prepare(
            "SELECT DISTINCT status , payed , delivered
             FROM orders
             WHERE id = :id
             LIMIT 1"
        );
        $query->execute(array('id' => $order_id));
        $order = $query->fetch();
        if($order->delivered == TRUE && $order->payed == TRUE){//if delivered and payed . set order status to Completed
            $query = $this->pdo->prepare(
                        "UPDATE orders
                         SET    status = :status
                         WHERE  id   = :id
                         LIMIT 1"
                    );
            $data_query = array(
                        "id"       => $order_id,
                        "status"   => 10
                    );
            $query->execute($data_query);

            $push = new PushController;
            $push->push_change_status_order($order_id, 10);
        }
    }

    private function return_order($order_id){

        $query = $this->pdo->prepare(
            "SELECT DISTINCT id, meal_id, quantity, sender_user_id, total_price, portion_price, status, created, delivery_type, type_payment, latitude, longitude  FROM orders
             WHERE id = :id
             LIMIT 1"
        );

        $query->execute(array('id' => $order_id));
        $order = $query->fetch();

        if($order == NULL){
            return $this->error_order;
        }

        $user_obj = new UserController;
        $meal_obj = new MealController;

        return array(
            'data' => array(
                'id'                => (int) $order->id,
                'client'            => $user_obj->get_user($order->sender_user_id),
                'phone'             => $this->get_user_phone($order->sender_user_id),
                //'receiver'          => $user_obj->get_user($receiver_id),
                'meal'              => $meal_obj->single_meal($order->meal_id)['data'],
                'quantity'          => (int) $order->quantity,
                'totalSum'          => (float) $order->total_price,
                'portionPrice'      => (float) $order->portion_price,
                'status'            => $order->status,
                'created'           => $order->created,
                'deliveryType'      => $order->delivery_type,
                'typePayment'       => $order->type_payment,
                'clientDeliveryInfo'=> $user_obj->get_delivery_info($order->sender_user_id),
                'latitude'          => $order->latitude,
                'longitude'         => $order->longitude,
                'comment'           => $this->order_comment($order_id)
            )
        );

    }



    /**
     * get orders list
     * @url GET /orders
     *
     */
    public function orders() {

        $list_orders = $this->construct_orders_full_for_user();

        $user_obj = new UserController;
        $meal_obj = new MealController;
        $data = $_GET['active'];

        foreach ($list_orders as $key => $order) {
            if($data == 'true'){
                if($order->status == 9 || $order->status == 8 || $order->status == 7 || $order->status == 10){
                    continue;
                }
            }
            if($data == 'false'){
                if($order->status == 1 || $order->status == 2 || $order->status == 3 || $order->status == 4 || $order->status == 5 || $order->status == 6){
                    continue;
                }
            }
            $result[] = array(
                                'id'                => (int) $order->id,
                                'client'            => $user_obj->get_user($order->sender_user_id),
                                'phone'             => $this->get_user_phone($order->sender_user_id),
                                //'receiver'          => $user_obj->get_user($order->chef_id),
                                'meal'              => $meal_obj->single_meal($order->meal_id)['data'],
                                'quantity'          => (int) $order->quantity,
                                'totalSum'          => (float) $order->total_price,
                                'portionPrice'      => (float) $order->portion_price,
                                'status'            => $order->status,
                                'created'           => $order->created,
                                'deliveryType'      => $order->delivery_type,
                                'typePayment'       => $order->type_payment,
                                'clientDeliveryInfo'=> $user_obj->get_delivery_info($order->sender_user_id),
                                'latitude'          => $order->latitude,
                                'longitude'         => $order->longitude,
                                'comment'           => $this->order_comment($order->id)

                        );
          }
          //return $result;
          return array('data' => $result);
    }



    private function construct_orders_full_for_user() {
        $user_id   = (int) $this->check_auth()->user_id;
        try{
            $query = $this->pdo->prepare(
                "SELECT DISTINCT * FROM orders
                 WHERE sender_user_id = :user_id
                 OR chef_id  		  = :user_id
                 ORDER BY created DESC"
            );

            $query->execute(array('user_id' => $user_id));
            $orders = $query->fetchAll(PDO::FETCH_OBJ);

        }catch (Exception $e) {
            return $e->getMessage();
        }
        return $orders;
    }


    /**
     * @url GET /messages
     *
     * @url POST /messages
     */
    public function messages($data)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
            return $this->set_order_message($data);



        $order_id = (int) $_GET['orderId'];
        $last_id  = (int) $_GET['lastId'];
        $sql     .= "SELECT * FROM order_messages WHERE order_id = :order_id";
        $params   = array('order_id' => $order_id);

        if ($last_id !== 0) {
            $sql .= " AND id > :id";
            $params['id'] = $last_id;
        }

        $sql .= " ORDER BY created ASC";

        $query = $this->pdo->prepare("$sql");
        $query->execute($params);
        $messages = $query->fetchAll(PDO::FETCH_OBJ);

        $order_messages = array();

        if ( ! $messages)
            $order_messages = null;

        foreach ($messages as $message) {
            $order_messages[] = array(
                'id'      => (int) $message->id,
                'userId'  => (int) $message->user_id,
                'created' => $message->created,
                'message' => $message->message,
                'system'  => (int) $message->system
            );
        }

        return array('data' => $order_messages);
    }




    private function set_order_message($data)
    {
        $user_id  = (int) $this->check_auth()->user_id;
        $order_id = (int) $data->orderId;
        $message  = html_entity_decode($this->validation->to_string($data->text), ENT_QUOTES);
        $created  = gmdate("Y-m-d H:i:s");

        if ( ! $data OR ! $message)
            return $this->error_invalid_input;

        $query = $this->pdo->prepare(
            "INSERT INTO order_messages
                (order_id, user_id, message, created)
             values
                (:order_id, :user_id, :message, :created)"
        );

        $params = array(
            "order_id" => $order_id,
            "user_id"  => $user_id,
            "message"  => $message,
            "created"  => $created
        );

        try {

            //update order unread status
            $query_update = $this->pdo->prepare(
                    "UPDATE orders
                     SET    unread_messages = True
                     WHERE  sender_user_id   = :user_id
                     AND    id = :id 
                     LIMIT 1"
            );
            $data_query_update = array(
                "user_id"             => (int)$user_id,
                "id"            => $order_id
            );
            $update = $query_update->execute($data_query_update);

            $insert = $query->execute($params);

            $push = new PushController;
            $push->push_new_message_user($order_id, $user_id, $message);

            return array(
                'data' => array(
                    'id'      => (int) $this->pdo->lastInsertId(),
                    'userId'  => (int) $user_id,
                    'created' => $created,
                    'message' => $message,
                    'system'  => NULL
                )
            );
        } catch (Exception $e) {
            return $this->error_message_create;
        }
    }



    private function order_comment($order_id)
    {
        $query = $this->pdo->prepare(
            "SELECT DISTINCT * FROM comments
             WHERE order_id = :order_id LIMIT 1"
        );

        $query->execute(array('order_id' => $order_id));
        $comment = $query->fetch();

        if ( ! $comment) return null;

        return array(
            'created' => $comment->created,
            'message' => $comment->text,
            'rating'  => (int) $comment->rating
        );
    }

    /*
    *  get buyer coordinates from profile if dont receive
    */
    private function get_userprofile_coordinates($sender_id)
    {
        $query = $this->pdo->prepare(
            "SELECT latitude, longitude
             FROM delivery
             WHERE user_id = :user_id
             LIMIT 1"
        );
        $query->execute(array('user_id' => $sender_id));
        $coordinates = $query->fetchObject();

        return $coordinates;
    }

    /**
     * @url GET /stripe_connect
     *
     */
    public function stripe_connect()
    {
    if (isset($_GET['code'])) { // Redirect w/ code

        //get type device
        $token = $_GET['state'];
        $chef_obj = $this->user_by_token($token);
        if($chef_obj == NULL){
            return $this->error_token;
        }
        $chef_id        = (int) $chef_obj->user_id;
        $device_type    = $chef_obj->device_type;//1 - Android , 2 - iOS
        /**
         * check for already connected stripe
         *
         * @var obj
         */
        $chef_stripe = $this->check_chef_stripe($chef_id);
        if($chef_stripe != NULL)
        {
            return $this->error_stripe_already_connected;
        }
        //sessions
  		$code = $_GET['code'];
        
		$token_request_body = array(
								    'grant_type' 	=> 'authorization_code',
								    'client_id' 	=> STRIPECLIENTID,
								    'code' 			=> $code,
								    'client_secret' => STRIPECLIENTSECRET
									);

		$ch = curl_init("https://connect.stripe.com/oauth/token");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true );
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

	    $respCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $responce = json_decode(curl_exec($ch), true);
	    //var_dump($responce);
        curl_close($ch);

        //update business info for chef
        $query = $this->pdo->prepare(
                    "UPDATE chefs
                     SET    stripe_client_id = :stripe_client_id, access_token = :access_token, refresh_token = :refresh_token
                     WHERE  user_id   = :user_id
                     LIMIT 1"
                );
                $data_query = array(
                    "user_id"              => (int)$chef_id,
                    "stripe_client_id"     => $responce['stripe_user_id'],
                    "access_token"         => $responce['access_token'],
                    "refresh_token"        => $responce['refresh_token'],
                );
                $result = $query->execute($data_query);


        header("Location: foodridge://stripeconnect/success?userId=$chef_id");

	} else if (isset($_GET['error'])) { // Error
       header("Location: foodridge://stripeconnect/failed?userId=$chef_id");
    } else {
       header("Location: foodridge://stripeconnect/failed?userId=$chef_id");
      }
	}

	protected function user_by_token($token){
        $query = $this->pdo->prepare(
            "SELECT device_type, user_id FROM sessions
             WHERE token = :token
             LIMIT 1"
        );
        $query->execute(array('token' => $token));
        $user_obj = $query->fetchObject();
        return $user_obj;
    }


    protected function make_payment($charge)
    {
    	\Stripe\Stripe::setApiKey(STRIPECLIENTSECRET);

		$ch = \Stripe\Charge::retrieve($charge);
		$result = $ch->capture();

		if($result->captured == true AND $charge == $result->id){
			$query = $this->pdo->prepare(
                        "UPDATE orders
                         SET    status = :status, payed = :payed
                         WHERE  transaktion_id   = :transaktion_id
                         LIMIT 1"
                    );
            $data_query = array(
                        "transaktion_id"   => $charge,
                        "status"   		   => 6,
                        "payed"   		   => 1
                    );
            $query->execute($data_query);

            $push = new PushController;
            $push->push_make_payment($charge, 6);
		}else{
			return $this->error_payment;
		}
	}

    //get user phone number
    protected function get_user_phone($user_id){
            $query = $this->pdo->prepare(
                                        "SELECT DISTINCT phone
                                         FROM users
                                         WHERE id = :id
                                         LIMIT 1"
                );
            $query->execute(array('id' => $user_id));
            $phone_number = $query->fetchColumn();
            return $phone_number;
    }

    /**
     * stripe disconnect account
     *
     * @url DELETE /stripe_disconnect
     */
    public function stripe_disconnect() 
    {
        $chef_id = (int) $this->check_auth()->user_id;
        $active_orders = $this->check_active_orders($chef_id);
        if($active_orders != NULL)
        {
            return $this->error_close_orders_first;
        }
        
        $query = $this->pdo->prepare(
                                "SELECT DISTINCT *
                                 FROM chefs
                                 WHERE user_id = :chef_id
                                 LIMIT 1"
            );
        $query->execute(array('chef_id' => $chef_id ));
        $chefObj = $query->fetchObject();

        $url = "https://connect.stripe.com/oauth/deauthorize";
        $data = array(
          'client_secret'  => STRIPECLIENTSECRET,
          'client_id'      => STRIPECLIENTID,
          'stripe_user_id' => $chefObj->stripe_client_id,
        );
        $body = http_build_query($data);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = json_decode(curl_exec($ch), true);
        
        //if($output['error'] == NULL)
        //{
            $query = $this->pdo->prepare(
                            "UPDATE chefs
                             SET    stripe_client_id = :stripe_client_id,
                                    access_token     = :access_token,
                                    refresh_token    = :refresh_token
                             WHERE  user_id   = :user_id
                             LIMIT 1"
                        );
            $data_query = array(
                            "user_id"          => $chef_id,
                            "stripe_client_id" => NULL,
                            "access_token"     => NULL,
                            "refresh_token"    => NULL
                        );
            $query->execute($data_query);
        //}else
        //{
        //    return $this->error_stripe_disconnect;    
        //}
        
        $user = new UserController;
        return array(
            'data' =>  $user->get_chef_profile($chef_id)
        );
        
    }

    /**
     * Check for active orders when trying to disconnect
     *
     * @return object
     */
    protected function check_active_orders($chef_id)
    {
        $status_list_to_check = "(1,2,3,4,5)";

        $query = $this->pdo->prepare(
                                    "SELECT DISTINCT *
                                     FROM orders
                                     WHERE chef_id = :chef_id
                                     AND status IN $status_list_to_check
                                     LIMIT 1"
                );
        $query->execute(array('chef_id' => $chef_id ));
        $order = $query->fetchObject();
        return $order;
    }

    /**
     * Check for already connected stripe
     *
     * @return object
     */
    protected function check_chef_stripe($chef_id)
    {
        $query = $this->pdo->prepare(
                                    "SELECT DISTINCT *
                                     FROM chefs
                                     WHERE user_id = :chef_id
                                     AND stripe_client_id != NULL
                                     LIMIT 1"
                );
        $query->execute(array('chef_id' => $chef_id ));
        $chef = $query->fetchObject();
        return $chef;
    }


















    /* TESTING METHODS ONLY*/
    
    /**
     * stripe
     *
     * @url GET /stripe_managed
     */
    public function stripe_managed() {
	\Stripe\Stripe::setApiKey(STRIPECLIENTSECRET);
	$res = \Stripe\Account::create(
	  array(
	    "country" => "AU",
	    "managed" => true
	  )
	);

	var_dump($res);
	
	}






    /**
     * stripe
     *
     * @url GET /stripe_test
     */
    public function stripe_test() {
        //$myCard = array('number' => '4242424242424242', 'exp_month' => 8, 'exp_year' => 2018);
        ?>
        <form action="http://foodridge.dev.rollncode.com/api/var_dump" method="POST">
          <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="pk_test_jVs7cj0XhdBCQa6Hnaw2NGcL"
            data-amount="999"
            data-name="test"
            data-description="Widget"
            data-image="/img/documentation/checkout/marketplace.png"
            data-locale="auto"
            data-zip-code="true">
          </script>
        </form>

        <?php
    }
    
    /**
     *  @url GET /var_dump
     *  @url POST /var_dump
     */
    public function var_dump($data)
    {
        var_dump($_POST);
    }

    /* TESTING METHODS ONLY*/
}
