<?php 
defined('SYSPATH') or die('No direct script access.');
ini_set('display_errors', 'Off');
ini_set('display_startup_errors', 'Off');
error_reporting(0);
/*
ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');
error_reporting(E_ALL);*/
require(SYSPATH.'/api/source/config/config.php');
require(SYSPATH.'/api/source/Classes/Validation.class.php');
include(SYSPATH.'/api/lib/stripe-php/init.php');
class Base
{   
    protected $settings = array('admin_email' => "info@foodridge.com"
    							//'admin_email' => "savchenko.artur90@gmail.com"
                                );


    protected $error_email            = array('error' => array('code' => 1, 'message' => 'invalid email'));
    protected $error_pushId           = array('error' => array('code' => 2, 'message' => 'invalid pushId'));
    protected $error_username         = array('error' => array('code' => 3, 'message' => 'invalid username'));
    protected $error_password         = array('error' => array('code' => 4, 'message' => 'invalid login'));
    protected $error_device_type      = array('error' => array('code' => 5, 'message' => 'invalid deviceType'));
    protected $error_social_type      = array('error' => array('code' => 6, 'message' => 'invalid social_type'));
    protected $error_social_user_id   = array('error' => array('code' => 7, 'message' => 'invalid social_user_id'));
    protected $error_social_token     = array('error' => array('code' => 8, 'message' => 'invalid social token'));
    protected $error_not_uniq_email   = array('error' => array('code' => 9, 'message' => 'email not unique'));
    protected $error_device_id        = array('error' => array('code' => 10, 'message' => 'invalid deviceId'));
    protected $error_social_login     = array("error" => array("code" => 11, "message" => "social login failed"));
    protected $error_delivery         = array("error" => array("code" => 12, "message" => "invalid delivery info"));
    protected $error_filetype         = array("error" => array("code" => 13, "message" => "invalid file type"));
    protected $error_upload           = array("error" => array("code" => 14, "message" => "upload file failed"));
    protected $error_phone            = array("error" => array("code" => 15, "message" => "invalid phone"));
    protected $error_push_flag        = array("error" => array("code" => 16, "message" => "invalid push flag"));
    protected $error_time             = array("error" => array("code" => 17, "message" => "incorrect time format"));
    protected $error_about            = array("error" => array("code" => 18, "message" => "invalid about"));
    protected $error_business_name    = array("error" => array("code" => 19, "message" => "invalid business name"));
    protected $error_business_site    = array("error" => array("code" => 20, "message" => "invalid business site"));
    protected $error_fileexist        = array("error" => array("code" => 21, "message" => "Can't find file"));
    protected $error_registrationId   = array("error" => array("code" => 22, "message" => "invalid registrationId"));
    protected $error_country          = array("error" => array("code" => 23, "message" => "invalid country"));
    protected $error_city             = array("error" => array("code" => 24, "message" => "invalid city"));
    protected $error_street           = array("error" => array("code" => 25, "message" => "invalid street"));
    protected $db_error               = array("error" => array("code" => 26, "message" => "database query failed"));
    protected $error_deleting_file    = array("error" => array("code" => 27, "message" => "error deleting file"));
    protected $error_follow           = array("error" => array("code" => 28, "message" => "incorrect data"));
    protected $error_meal_id          = array("error" => array("code" => 29, "message" => "missed id parameter of meal"));
    protected $cuisines_error         = array("error" => array("code" => 30, "message" => "cuisine id incorrect"));
    protected $chefprofile_error      = array("error" => array("code" => 31, "message" => "chef profile save error"));
    protected $error_json             = array("error" => array("code" => 32, "message" => "invalid json format"));

    protected $error_token             = array("error" => array("code" => 33, "message" => "invalid foodridge access token"));
    protected $error_payment           = array("error" => array("code" => 34, "message" => "payment error"));
    protected $error_invalid_status    = array("error" => array("code" => 35, "message" => "invalid order status to update"));
    protected $error_chef_exist_stripe = array("error" => array("code" => 36, "message" => "chef stripe account deauthorized"));
    protected $error_portions_number   = array("error" => array("code" => 37, "message" => "not enought portions to sell"));
    protected $error_total_price       = array("error" => array("code" => 38, "message" => "total sum doesn't match"));
    protected $error_fill_delivery     = array("error" => array("code" => 39, "message" => "user must fill delivery address"));

    protected $errors_required           = array("error" => array("code" => 40, "message" => "required params"));
    protected $error_meal_name           = array('error' => array('code' => 41, 'message' => 'invalid meal name'));
    protected $error_description         = array('error' => array('code' => 42, 'message' => 'invalid description'));
    protected $error_other               = array('error' => array('code' => 43, 'message' => 'invalid other'));
    protected $role_error                = array('error' => array('code' => 44, 'message' => 'operation not permitted to simple user'));
    protected $ingredient_error          = array('error' => array('code' => 45, 'message' => 'invalid ingredient id'));
    protected $error_requered_meal       = array('error' => array('code' => 47, 'message' => 'parameter mealId is required for update'));
    protected $error_set_follow          = array("error" => array("code" => 48, "message" => "you already follow"));
    protected $error_owner_media         = array("error" => array("code" => 49, "message" => "getting file or permission error"));
    protected $error_order_create        = array("error" => array("code" => 50, "message" => "error creating order"));
    protected $error_order               = array('error' => array('code' => 51, 'message' => 'not found order'));
    protected $error_no_permission       = array('error' => array('code' => 52, 'message' => 'user not permitted to order'));
    protected $error_invalid_input       = array('error' => array('code' => 53, 'message' => 'invalid input params'));
    protected $error_message_create      = array('error' => array('code' => 54, 'message' => 'invalid create message'));

    protected $error_create_order_params      = array('error' => array('code' => 55, 'message' => 'some of required params to create order missed. dishId,quantity,deliveryType,totalSum'));
    protected $error_update_order_params      = array('error' => array('code' => 56, 'message' => 'required params orderId OR orderStatus missed'));

    protected $error_comment_exists    = array('error' => array('code' => 57, 'message' => 'comment already exists'));
    protected $error_no_comments       = array('error' => array('code' => 58, 'message' => 'no comment'));
    protected $error_invalid_meal_id   = array('error' => array('code' => 59, 'message' => 'invalid dishId'));
    protected $error_refund            = array('error' => array('code' => 60, 'message' => 'refund error'));

    protected $error_send_email        = array('error' => array('code' => 61, 'message' => 'error send email'));
    protected $error_list_without_stripe    = array('error' => array('code' => 62, 'message' => 'error list param without stripe connect'));
    protected $error_required_chef_profile  = array('error' => array('code' => 63, 'message' => 'name and phone required for creating chef'));
    protected $error_order_closed           = array('error' => array('code' => 64, 'message' => 'order closed'));
    protected $error_delivery_mismatch      = array('error' => array('code' => 65, 'message' => 'invalid delivery type'));
    protected $error_required_order_id      = array('error' => array('code' => 66, 'message' => 'order id required'));
    protected $error_required_phone         = array('error' => array('code' => 67, 'message' => 'phone required'));
    protected $error_required_text          = array('error' => array('code' => 68, 'message' => 'feedback text required'));
    protected $error_ingredients            = array('error' => array('code' => 69, 'message' => 'ingredients error'));
    protected $error_cuisines               = array('error' => array('code' => 70, 'message' => 'cuisines error'));
    protected $error_mealId_exist           = array('error' => array('code' => 71, 'message' => 'meal doesnt exist'));
    protected $error_facebook               = array('error' => array('code' => 72, 'message' => 'Your facebook email unconfirmed. Sign in to another Facebook account or confirm current Facebook account'));
    protected $error_close_orders_first     = array('error' => array('code' => 73, 'message' => 'close your orders first'));
    protected $error_stripe_already_connected   = array('error' => array('code' => 74, 'message' => 'stripe already connected'));
    protected $error_stripe_disconnect          = array('error' => array('code' => 75, 'message' => 'this application is not connected to stripe account'));
    protected $error_required_type_payment      = array('error' => array('code' => 76, 'message' => 'typePayment required'));
    protected $error_coordinates                = array('error' => array('code' => 77, 'message' => 'coordinates invalid'));
    protected $error_delivery_required          = array('error' => array('code' => 78, 'message' => 'delivery user coordinates required.'));
    protected $error_name_required              = array('error' => array('code' => 79, 'message' => 'name required.'));
    protected $error_chef_addr_required         = array('error' => array('code' => 80, 'message' => 'chef must have adress, when cash pay'));
    protected $error_firebase_id_exist          = array('error' => array('code' => 81, 'message' => 'this InviteID already exist'));
    protected $error_already_follow             = array('error' => array('code' => 82, 'message' => 'user followed already'));
    protected $error_coupon_limit               = array('error' => array('code' => 83, 'message' => 'coupon limit for chef reached'));
    protected $error_required_coupon_entity     = array('error' => array('code' => 84, 'message' => 'required fields couponName , discount , dateTo , dateFrom'));
    protected $error_not_valid_dates            = array('error' => array('code' => 85, 'message' => 'Choose correct dates to activate discount'));
    protected $error_maximum_discount           = array('error' => array('code' => 86, 'message' => 'Discount must be less that 80%'));
    protected $error_with_discount_mismatch     = array('error' => array('code' => 87, 'message' => 'Total sum with discount mismatch'));
    protected $error_with_no_discount_mismatch  = array('error' => array('code' => 88, 'message' => 'Total sum with no discount mismatch'));
    protected $error_not_active_coupon          = array('error' => array('code' => 89, 'message' => 'Coupon must be active to payment with coupons'));
    protected $error_pushFrequency              = array('error' => array('code' => 90, 'message' => 'pushFrequency required param'));
    

    protected $pdo        = null;
    protected $role_chef  = 1;
    protected $role_user  = 2;
    protected $validation;

    function __construct()
    {
        $this->validation = new Validation;

        $opt = array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_LAZY
        );

        // check connect DB
        try {
            if ($this->pdo == null)
                $this->pdo = new PDO(DSN, USER, PASS, $opt);
        } catch (Exception $e) {
            $this->pdo = null;
            header('HTTP/1.0 503 Service Unavailable');
            exit;
        }

        $this->check_auth();

    }


    protected function check_auth()
    {
        $head   = getallheaders();
        $uid    = ($head['uid']) ? (int) $head['uid'] : false;
        $token  = ($head['token']) ? $this->validation->is_md5($head['token']) : false;

        if ($_SERVER['REQUEST_URI'] == '/api/login' OR $_SERVER['REQUEST_URI'] == '/api/register' OR  strtok($_SERVER["REQUEST_URI"],'?') == '/api/stripe_connect' OR  strtok($_SERVER["REQUEST_URI"],'?') == '/api/stripe' OR $_SERVER['REQUEST_URI'] == '/api/check_email_by_token') {
        //if(true){
            $auth = true;
            $term_flag = true;
        } else {
            $query = $this->pdo->prepare(
                "SELECT DISTINCT token, user_id
                 FROM sessions
                 WHERE token = :token AND user_id = :user_id
                 LIMIT 1"
            );
            $query->execute(array('token' => $token, 'user_id' => $uid));
            $auth = $query->fetch();
            $auth = ( ! $auth) ? false : $auth;
        }

        if ( ! $auth) {
            header('HTTP/1.0 403 Forbidden');
            exit;
        }

        //terms part
        if ($_SERVER['REQUEST_URI'] == '/api/terms'){
            $term_flag = true;
        }
        if($term_flag != true){

            $query = $this->pdo->prepare(
                    "SELECT DISTINCT terms
                     FROM users
                     WHERE id = :id
                     LIMIT 1"
                );
            $query->execute(array('id' => $uid));
            $terms_obj = $query->fetchObject();

            $term_flag = $terms_obj->terms;
            if($term_flag == false){
                header('HTTP/1.0 424 Failed Dependency');
                exit;
            }
        }
        //terms part

        return $auth;
    }
}
