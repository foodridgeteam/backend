<?php

class RefundController extends Base
{
    function __construct() {
    	parent::__construct();
	}

    public function stripe_make_refund($charge, $order_id)
    {   
        \Stripe\Stripe::setApiKey(STRIPECLIENTSECRET);

        try{
            $refund_arr = \Stripe\Refund::create(array(
              "charge" => $charge,
              "refund_application_fee" => TRUE,
              "reverse_transfer" => TRUE
            ));
        }catch (Exception $e) {
            return $this->error_refund;
        }
        if($refund_arr['charge'] == $charge AND $refund_arr['status'] == "succeeded"){
            $query = $this->pdo->prepare(
                        "UPDATE orders
                         SET    payed = :payed
                         WHERE  transaktion_id   = :transaktion_id
                         LIMIT 1"
                    );
            $data_query = array(
                        "transaktion_id"   => $charge,
                        "payed"            => 0
                    );
            $query->execute($data_query);

            //insert refund data for statistic
            $query = $this->pdo->prepare(
            "INSERT INTO order_refunds
                (order_id, amount, refund_id , charge_id, created)
             values
                (:order_id, :amount, :refund_id , :charge_id, :created)"
            );

            $data = array(
                "order_id"     => $order_id,
                "amount"       => $refund_arr['amount']/100,
                "refund_id"    => $refund_arr['id'],
                "charge_id"    => $refund_arr['charge'],
                "created"      => gmdate("Y-m-d H:i:s"),
            );
            $result     = $query->execute($data);
            return true;
        }
    }

    protected function check_auth(){
        return true;
    }
	
}
