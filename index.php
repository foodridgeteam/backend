<?php $syspath = $_SERVER["DOCUMENT_ROOT"];
define(SYSPATH, $syspath);
require __DIR__ . '/source/Jacwright/RestServer/RestServer.php';
require __DIR__ . '/source/Controllers/UserController.php';
require __DIR__ . '/source/Controllers/MealController.php';
require __DIR__ . '/source/Controllers/MediaController.php';
require __DIR__ . '/source/Controllers/ForgotController.php';
require __DIR__ . '/source/Controllers/PushController.php';
require __DIR__ . '/source/Controllers/OrderController.php';
require __DIR__ . '/source/Controllers/RefundController.php';
require __DIR__ . '/source/Controllers/CouponController.php';


$server = new \Jacwright\RestServer\RestServer('');
$server->addClass('UserController');
$server->addClass('MealController');
$server->addClass('MediaController');
$server->addClass('ForgotController');
$server->addClass('PushController');
$server->addClass('OrderController');
$server->addClass('RefundController');
$server->addClass('CouponController');

$server->handle();


